﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;

namespace AvitoStarBot.Interfaces
{
    public interface IWelcomableDialog
    {
        Task SendWelcomeCardAsync(ITurnContext turnContext, CancellationToken cancellationToken);
    }
}
