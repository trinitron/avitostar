﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.


using System;
using Microsoft.Bot.Builder.Dialogs;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using AvitoModels.Sockets;
using AvitoStarBot.Extensions;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;

namespace AvitoStarBot.Dialogs
{
    public class ClientListDialog : ComponentDialog
    {
        private readonly IStorage _iStorage;

        public ClientListDialog(IStorage iStorage) : base(nameof(ClientListDialog))
        {
            _iStorage = iStorage;
            InitialDialogId = nameof(WaterfallDialog);
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginFormflowAsync,
                ResumeReplyUserDialodAsync,
            }));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
        }

        private async Task<DialogTurnResult> BeginFormflowAsync(
            WaterfallStepContext stepContext,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var botUserGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToGuid().ToString();
            var memoryItem = await _iStorage.ReadAsync(new[] { botUserGuid }, cancellationToken);
            var userMessages = (List<AvitoMessageIn>)memoryItem.FirstOrDefault().Value;


            if (memoryItem.Count > 0)
            {
                var buttonChoices = new List<CardAction>();
                var responseChoices = new List<Choice>();
                foreach (var msg in userMessages.ToArray().Reverse())
                {
                    var tDate = msg.value.created.TryToConvertDate("HH:mm");
                    var mText = msg.value.body.text ?? "-";
                    string mTextShort = mText.Truncate(32);
                    var row = String.Format("{1} ({0}): {2}", msg.value.fromUid, tDate, mTextShort);

                    responseChoices.Add(new Choice() { Value = msg.value.fromUid.ToString(), Synonyms = new List<string>() { row } });
                    buttonChoices.Add(new CardAction(ActionTypes.ImBack, row, value: msg.value.fromUid.ToString()));


                }

                responseChoices.Add(new Choice() { Value = "n:AccReturn", Synonyms = new List<string>() { "Назад" } });
                buttonChoices.Add(new CardAction(ActionTypes.ImBack, "Назад", value: "n:AccReturn"));

                /* ADAPTIVE CARDS: https://www.npmjs.com/package/adaptivecards
                https://blog.botframework.com/2019/07/02/using-adaptive-cards-with-the-microsoft-bot-framework/
                var card2 = new AdaptiveCard(new AdaptiveSchemaVersion(1, 0))
                {
                    // Use LINQ to turn the choices into submit actions
                    Actions = choices.Select(choice => new AdaptiveSubmitAction
                    {
                        Title = choice,
                        Data = choice,  // This will be a string
                    }).ToList<AdaptiveAction>(),
                };*/

                var card = new HeroCard() { Text = "Выберите клиента для продолжения диалога", Buttons = buttonChoices };
                var activity = (Activity)MessageFactory.Attachment(card.ToAttachment());
                return await stepContext.PromptAsync(nameof(ChoicePrompt), new PromptOptions { Prompt = activity, Choices = responseChoices, Style = ListStyle.None }, cancellationToken);

            }
            else
            {
                await stepContext.Context.SendActivityAsync("Информация о входящих пока отсутствует");
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
        }



        private async Task<DialogTurnResult> ResumeReplyUserDialodAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var optionSelected = (stepContext.Result as FoundChoice);
            if (optionSelected.Value != "n:AccReturn")
            {
                var botUserGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToGuid().ToString();
                var memoryItem = await _iStorage.ReadAsync(new[] { botUserGuid }, cancellationToken);
                var userMessages = (List<AvitoMessageIn>)memoryItem.FirstOrDefault().Value;
                //var message = userMessages.ToArray().Reverse().ElementAtOrDefault(optionSelected.Index);
                var message = userMessages.ToArray().Reverse().FirstOrDefault(x => x.value.fromUid == int.Parse(optionSelected.Value));

                return await stepContext.BeginDialogAsync(nameof(ReplyUserDialog), message, cancellationToken);
            }
            return await stepContext.ReplaceDialogAsync(nameof(RootDialog), default, cancellationToken);

        }
    }
}
