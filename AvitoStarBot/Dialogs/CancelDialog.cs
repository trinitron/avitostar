﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;

namespace AvitoStarBot.Dialogs
{
    public class CancelDialog : ComponentDialog
    {
        public CancelDialog() : base(nameof(CancelDialog))
        {
            // Initialize our dialogs and prompts
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                EndDialogAsync
            }));

            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await stepContext.PromptAsync(nameof(ConfirmPrompt), new PromptOptions { Prompt = MessageFactory.Text("Вы действительно хотите вернуться назад?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> EndDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = (bool)stepContext.Result;
            return await stepContext.EndDialogAsync(result, cancellationToken: cancellationToken);
        }
    }
}
