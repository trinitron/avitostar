﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Sockets;
using AvitoStarBot.Extensions;

namespace AvitoStarBot.Dialogs
{
    public class ReplyUserDialog : ComponentDialog
    {
        private readonly CloudQueueClient _queueClient;
        private readonly IStorage _iStorage;

        public ReplyUserDialog(CloudQueueClient queueClient, IStorage iStorage)
            : base(nameof(ReplyUserDialog))
        {
            _iStorage = iStorage;
            _queueClient = queueClient;

            // Initialize our dialogs and prompts.
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                SubmitRequestAsync,
            }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Что ответить пользователю?"), }, cancellationToken);
        }

        private async Task<DialogTurnResult> SubmitRequestAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var text = stepContext.Result as string;
            var uMessage = stepContext.Options as AvitoMessageIn;

            await _queueClient.EnqueueAvitoMessage(text, uMessage.value.channelId, uMessage.value.uid);
            await stepContext.Context.SendActivityAsync("✉️ Сообщение отправлено", cancellationToken: cancellationToken);

            return await stepContext.ReplaceDialogAsync(nameof(RootDialog), default, cancellationToken);
        }

    }
}