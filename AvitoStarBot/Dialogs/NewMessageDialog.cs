﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Sockets;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.WindowsAzure.Storage.Queue;

namespace AvitoStarBot.Dialogs
{
    public class NewMessageDialog : ComponentDialog
    {
        private readonly CloudQueueClient _queueClient;
        private readonly IStorage _iStorage;

        public NewMessageDialog(/*CloudQueueClient queueClient, IStorage iStorage*/)
            : base(nameof(NewMessageDialog))
        {
            //_iStorage = iStorage;
            //_queueClient = queueClient;

            // Initialize our dialogs and prompts
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                RepeatDialogAsync
            }));

            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var uMessage = stepContext.Options as AvitoMessageIn;
            if (uMessage != null)
            {
                //var info = String.Format("Новое сообщение от пользователя **{0}**", uMessage.value.fromUid);

                var adLink = "https://www.avito.ru/" + uMessage.value.AdId;
                var info = String.Format("🙋‍ Новое сообщение по объявлению #[{1}]({0})", adLink, uMessage.value.AdId);
                await stepContext.Context.SendActivityAsync(info, cancellationToken: cancellationToken);

                var msg = String.Format("💬 *{0}*", uMessage.value.body.text);
                await stepContext.Context.SendActivityAsync(msg, cancellationToken: cancellationToken);

                var activity = MessageFactory.Text("Получено новое сообщение, ответить?", "новое сообщение", InputHints.ExpectingInput);
                activity.SuggestedActions = new SuggestedActions
                {
                    Actions = new List<CardAction>
                    {
                        new CardAction {Title = "Ответить однократно", Type = ActionTypes.ImBack, Value = "n:ReplyOnce"},
                        new CardAction {Title = "Начать переписку", Type = ActionTypes.ImBack, Value = "n:ReplyLoop"},
                        new CardAction {Title = "О пользователе", Type = ActionTypes.ImBack, Value = "n:ClientInfo"},
                        new CardAction {Title = "Отмена", Type = ActionTypes.ImBack, Value = "n:AccReturn"}
                    }
                };

                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = activity }, cancellationToken);
            }

            return await stepContext.EndDialogAsync(null, cancellationToken);
        }

        private async Task<DialogTurnResult> RepeatDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            /*
            var botUserGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToGuid().ToString();
            var memoryItem = await _iStorage.ReadAsync(new[] { botUserGuid }, cancellationToken);
            var userMessages = (List<AvitoMessageIn>)memoryItem.FirstOrDefault().Value;
            var lastChid = userMessages.LastOrDefault()?.value.channelId;
            */

            switch (stepContext.Context.Activity.Value ?? stepContext.Context.Activity.Text)
            {
                case "n:ReplyOnce":
                case "Ответить однократно":
                    return await stepContext.BeginDialogAsync(nameof(ReplyUserDialog), stepContext.Options, cancellationToken);
                case "n:ReplyLoop":
                case "Начать переписку":
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("В режиме переписки все последующие сообщения будут моментально отправляться пользователю. Напечатай **?** для выхода"), cancellationToken);
                    return await stepContext.BeginDialogAsync(nameof(LoopUserDialog), stepContext.Options, cancellationToken);
                case "О пользователе":
                case "n:ClientInfo":
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("Информация доступна только для премиум пользователей!"), cancellationToken);
                    await stepContext.EndDialogAsync(null, cancellationToken);
                    return await stepContext.ContinueDialogAsync(cancellationToken);
                case "n:AccReturn":
                case "Отмена":
                    await stepContext.CancelAllDialogsAsync(cancellationToken);
                    return await stepContext.ContinueDialogAsync(cancellationToken);
                default:
                    return await stepContext.BeginDialogAsync(nameof(ReplyUserDialog), stepContext.Options, cancellationToken);
            }
        }

    }
}
