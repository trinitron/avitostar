﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Sockets;
using AvitoStarBot.Extensions;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.WindowsAzure.Storage.Queue;

namespace AvitoStarBot.Dialogs
{
    public class LoopUserDialog : ComponentDialog
    {
        private readonly CloudQueueClient _queueClient;
        private readonly IStorage _iStorage;

        public LoopUserDialog(CloudQueueClient queueClient, IStorage iStorage)
            : base(nameof(LoopUserDialog))
        {
            _iStorage = iStorage;
            _queueClient = queueClient;

            // Initialize our dialogs and prompts
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                RepeatDialogAsync
            }));

            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            return new DialogTurnResult(DialogTurnStatus.Waiting);
        }

        private async Task<DialogTurnResult> RepeatDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var text = stepContext.Result as string;
            var uMessage = stepContext.Options as AvitoMessageIn;
            await _queueClient.EnqueueAvitoMessage(text, uMessage.value.channelId, uMessage.value.uid);

            // Otherwise, repeat this dialog, passing in the list from this iteration.
            return await stepContext.ReplaceDialogAsync(nameof(LoopUserDialog), stepContext.Options, cancellationToken);
        }
    }
}
