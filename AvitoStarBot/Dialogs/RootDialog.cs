﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AvitoStarBot.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace AvitoStarBot.Dialogs
{
    public class RootDialog : ComponentDialog, IWelcomableDialog
    {

        private ILogger<RootDialog> _logger;
        private IHostingEnvironment _env;

        public RootDialog(ILogger<RootDialog> logger, IHostingEnvironment env, IServiceProvider serviceProvider)
            : base(nameof(RootDialog))
        {
            _logger = logger;
            _env = env;

            // Initialize our dialogs and prompts
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                ShowChildDialogAsync,
                ResumeAfterAsync,
            }));

            AddDialog((AuthorizeDialog)serviceProvider.GetService(typeof(AuthorizeDialog)));
            AddDialog((ClientListDialog)serviceProvider.GetService(typeof(ClientListDialog)));
            AddDialog((AccountSettingsDialog)serviceProvider.GetService(typeof(AccountSettingsDialog)));
            AddDialog((CancelDialog)serviceProvider.GetService(typeof(CancelDialog)));
            //Notifier
            AddDialog((NewMessageDialog)serviceProvider.GetService(typeof(NewMessageDialog)));
            AddDialog((ReplyUserDialog)serviceProvider.GetService(typeof(ReplyUserDialog)));
            AddDialog((LoopUserDialog)serviceProvider.GetService(typeof(LoopUserDialog)));

            //Service
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            InitialDialogId = nameof(WaterfallDialog);

            

        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var promptMessage = CreateTaskPromptMessageWithActions1("Ожидай новых сообщений или выбери действие из предложенных ниже:");
            await stepContext.Context.SendActivityAsync(promptMessage, cancellationToken);
            return new DialogTurnResult(DialogTurnStatus.Waiting);
            //return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
        }

        private async Task<DialogTurnResult> ShowChildDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            switch (stepContext.Context.Activity.Value ?? stepContext.Context.Activity.Text)
            {
                case "m:AddAccount":
                case "Подключить аккаунт":
                    return await stepContext.BeginDialogAsync(nameof(AuthorizeDialog), default, cancellationToken);
                case "m:MyClients":
                case "Мои клиенты":
                    return await stepContext.BeginDialogAsync(nameof(ClientListDialog), default, cancellationToken);
                case "m:MySettings":
                case "Мои аккаунты":
                    return await stepContext.BeginDialogAsync(nameof(AccountSettingsDialog), default, cancellationToken);
                case "m:MyApp":
                case "Настройки":
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("Изменение настроек доступно только для премиум пользователей!"), cancellationToken);
                    return await stepContext.ContinueDialogAsync(cancellationToken);
            }

            // We shouldn't get here, but fail gracefully if we do.
            await stepContext.Context.SendActivityAsync("Отправь любой текст или напечатай **?** для возврата в главное меню", cancellationToken: cancellationToken);
            // Continue through to the next step without starting a child dialog.
            return await stepContext.NextAsync(cancellationToken: cancellationToken);
        }



        private async Task<DialogTurnResult> ResumeAfterAsync(WaterfallStepContext stepContext,CancellationToken cancellationToken = default(CancellationToken))
        {
            // Даем пользователю время на прочтение сообщения из предшествующего диалога
            await Task.Delay(TimeSpan.FromMilliseconds(4000), cancellationToken);
            
            // Зацикливание диалога (начинаем диалог заного)
            return await stepContext.ReplaceDialogAsync(InitialDialogId, default, cancellationToken);

            // Ожидание пользовательского ввода, а затем запуск повторного диалога
            // return await stepContext.EndDialogAsync(null, cancellationToken);
        }


        public DialogContext DialogContext { get; set; }
        protected override async Task<DialogTurnResult> OnContinueDialogAsync(DialogContext innerDc, CancellationToken cancellationToken = default)
        {
            var dialogstack = innerDc?.Stack.Select(x => x.Id.ToString()).Aggregate((a, b) => a + " <= " + b);
            _logger.LogInformation("OnContinueDialogAsync.DialogStack: " + dialogstack);

            DialogContext = innerDc; //хак
            var result = await InterruptAsync(innerDc, cancellationToken);
            if (result != null)
            {
                return result;
            }

            return await base.OnContinueDialogAsync(innerDc, cancellationToken);
        }

        /// <summary>
        /// Сохраняем контекст для работы из NotifyController
        /// </summary>
        /// <param name="dc"></param>
        /// <returns></returns>
        protected override Task OnInitializeAsync(DialogContext dc)
        {
            DialogContext = dc;
            return Task.CompletedTask;
        }

        public async Task<DialogTurnResult> OnContinueDialogAsyncPublic(DialogContext innerDc, CancellationToken cancellationToken = default)
        {
            return await base.OnContinueDialogAsync(innerDc, cancellationToken);
        }

        private async Task<DialogTurnResult> InterruptAsync(DialogContext innerDc, CancellationToken cancellationToken)
        {
            if (innerDc.Context.Activity.Type == ActivityTypes.Message)
            {
                var text = innerDc.Context.Activity.Text.ToLowerInvariant();
                switch (text)
                {
                    case "help":
                    case "sos":
                    case "отмена":
                    case "назад":
                    case "n:AccReturn":
                    case "?":
                    case "/":
                        await innerDc.BeginDialogAsync(nameof(CancelDialog), default, cancellationToken);
                        return new DialogTurnResult(DialogTurnStatus.Waiting);
                    case "/start":
                        await SendWelcomeCardAsync(innerDc.Context, cancellationToken);
                        //await innerDc.CancelAllDialogsAsync(cancellationToken);
                        return null;
                }
            }

            if (innerDc.Context.Activity.Type == ActivityTypes.Message && innerDc.ActiveDialog?.Id == nameof(CancelDialog))
            {
                var text = innerDc.Context.Activity.Text.ToLowerInvariant();
                switch (text)
                {
                    case "yes":
                    case "да":
                        await innerDc.CancelAllDialogsAsync(cancellationToken);
                        return await innerDc.ReplaceDialogAsync(InitialDialogId, default, cancellationToken);
                }
            }
            return null;
        }

        private IMessageActivity CreateTaskPromptMessageWithActions(string messageText)
        {
            //var activity = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
            messageText = String.IsNullOrWhiteSpace(messageText) ? null : messageText;

            var acts = new List<CardAction>
            {
                new CardAction
                {
                    Title = "Подключить аккаунт",
                    Type = ActionTypes.ImBack,
                    Value = "m:AddAccount"
                },
                new CardAction
                {
                    Title = "Мои аккаунты",
                    Type = ActionTypes.ImBack,
                    Value = "m:MySettings"
                },
                new CardAction
                {
                    Title = "Мои клиенты",
                    Type = ActionTypes.ImBack,
                    Value = "m:MyClients"
                }
            };

            var activity = MessageFactory.SuggestedActions(acts, messageText);
            return activity;
        }

        private IMessageActivity CreateTaskPromptMessageWithActions1(string messageText)
        {
            var choices = new[] { "Подключить аккаунт", "Мои аккаунты", "Мои клиенты", "Настройки" };
            var card = new HeroCard { Text = messageText, Buttons = choices.Select(choice => new CardAction(ActionTypes.ImBack, choice, value: choice)).ToList(), };
            var activity = (Activity)MessageFactory.Attachment(card.ToAttachment());
            return activity;
        }

        public async Task SendWelcomeCardAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var card = new HeroCard();
            card.Title = "Привет, меня зовут **Avito Star Bot**";
            card.Text = @"Я помогу тебе с пересылкой сообщений авито в твой Telegram, " +
                        // (ограничение в 200 символов для heroCard телеграм)
                        //"Подключи свой кабинет авито, и я помогу тебе отвечать на сообщения от пользователей и вести с ними переписку. \n\n" +
                        "Подключи свой кабинет авито и я буду присылать сообщения от пользователей и вести с ними переписку.\n\n" +
                        "Если возникают вопросы, то просто напечатай ** ? ** для выхода в главное меню.";


            var host = new Uri(Environment.GetEnvironmentVariable("WEBSITE_HOSTNAME") != null ? ("https://" + Environment.GetEnvironmentVariable("WEBSITE_HOSTNAME")) : _env.WebRootPath + "/");
            var img = new Uri(host, "Sellavi23.jpg");
            card.Images = new List<CardImage>() { new CardImage(img.AbsoluteUri) };

            /*
            card.Buttons = new List<CardAction>()
            {
                new CardAction(ActionTypes.OpenUrl, "Get an overview", null, "Get an overview", "Get an overview", "https://docs.microsoft.com/en-us/azure/bot-service/?view=azure-bot-service-4.0"),
                new CardAction(ActionTypes.OpenUrl, "Ask a question", null, "Ask a question", "Ask a question", "https://stackoverflow.com/questions/tagged/botframework"),
                new CardAction(ActionTypes.OpenUrl, "Learn how to deploy", null, "Learn how to deploy", "Learn how to deploy", "https://docs.microsoft.com/en-us/azure/bot-service/bot-builder-howto-deploy-azure?view=azure-bot-service-4.0"),
            };
            */

            var response = MessageFactory.Attachment(card.ToAttachment());
            await turnContext.SendActivityAsync(response, cancellationToken);
        }
    }
}
