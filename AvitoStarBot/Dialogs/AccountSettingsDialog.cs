﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.


using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Bot.Builder.Dialogs;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Enums;
using AvitoModels.TableEntities;
using AvitoStarBot.Extensions;
using CloudTableExtensions;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Extensions.Configuration;

namespace AvitoStarBot.Dialogs
{
    public class AccountSettingsDialog : ComponentDialog
    {
        private const string SelectedAccount = "selectedAccount";
        private readonly CloudTableClient _tableClient;
        private readonly CloudBlobClient _cloudBlobClient;
        private IEnumerable<AvitoConnection> _userAccounts;
        private IConfiguration _iconfig;

        public AccountSettingsDialog(CloudTableClient tableClient, IConfiguration iconfig, CloudBlobClient cloudBlobClient) : base(nameof(AccountSettingsDialog))
        {
            _tableClient = tableClient;
            _cloudBlobClient = cloudBlobClient;
            _iconfig = iconfig;

            // Initialize our dialogs and prompts
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                PerformAccountAction,
                EndDialogAsync,
            }));

            //AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            InitialDialogId = nameof(WaterfallDialog);

        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var botUserGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToGuid().ToString();
            var searchPredicate = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, botUserGuid);
            var entities = await CloudTableUtils.ExecuteQuerySegmentedAsync<AvitoConnection>(_tableClient, new TableQuery().Where(searchPredicate));
            _userAccounts = entities; ///??????????

            if (entities.Any())
            {
                var Actions = new List<CardAction>();
                foreach (var entity in entities)
                {
                    if (entity != null)
                    {
                        var moscowTimeCreated = TimeZoneInfo.ConvertTime(entity.ConnectionStarted, TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
                        var row = entity.ConnectionStatus == 0 ? "✔️ " : "❌ ";
                        row += "Кабинет №" + entity.RowKey;
                        Actions.Add(new CardAction(ActionTypes.ImBack, row, value: entity.RowKey));
                    }

                }
                Actions.Add(new CardAction(ActionTypes.ImBack, "Назад", value: "n:AccReturn"));

                var card = new HeroCard { Text = "Выберите аккаунт для выполнения действий", Buttons = Actions };
                var activity = (Activity)MessageFactory.Attachment(card.ToAttachment());
                await stepContext.Context.SendActivityAsync(activity, cancellationToken);
                return new DialogTurnResult(DialogTurnStatus.Waiting);
            }
            else
            {
                await stepContext.Context.SendActivityAsync("У вас нет подключённых аккаунтов, выберите **подключить аккаунт** в главном меню", cancellationToken: cancellationToken);
                return await stepContext.EndDialogAsync(null, cancellationToken);

            }

        }

        private async Task<DialogTurnResult> PerformAccountAction(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (int.TryParse(stepContext.Result.ToString(), out int accountId))
            {
                stepContext.Values[SelectedAccount] = accountId;

                var activity = MessageFactory.Text("Что необходимо сделать?", "настройки аккаунта", InputHints.ExpectingInput);
                activity.SuggestedActions = new SuggestedActions
                {
                    Actions = new List<CardAction>
                    {
                        new CardAction
                        {
                            Title = "Переподключить",
                            Type = ActionTypes.ImBack,
                            Value = "n:AccReconnect"
                        },
                        new CardAction
                        {
                            Title = "Отключить",
                            Type = ActionTypes.ImBack,
                            Value = "n:AccDisconnect"
                        },
                        new CardAction
                        {
                            Title = "Назад",
                            Type = ActionTypes.ImBack,
                            Value = "n:AccReturn"
                        },
                    }
                };

                //return Task.FromResult(stepContext)
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = activity }, cancellationToken);
            }
            else
            {
                //await stepContext.Context.SendActivityAsync($"Sorry, I did not find any application with the name '{stepContext.Result}'.",cancellationToken: cancellationToken);
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> EndDialogAsync(
            WaterfallStepContext stepContext,
            CancellationToken cancellationToken = default(CancellationToken))
        {

            var accId = (int)stepContext.Values[SelectedAccount];
            var selectedAcc = _userAccounts.FirstOrDefault(x => x.RowKey == accId.ToString());
            if (selectedAcc != null)
            {
                switch (stepContext.Context.Activity.Value ?? stepContext.Context.Activity.Text)
                {

                    case "n:AccReconnect":
                    case "Переподключить":
                        // Если учетная запись отключена пользователем, а затем повторно включается, авторизовываться повторно не требуется
                        selectedAcc.ConnectionStatus = (int)ConnectionStatusCodes.ActiveConnection;
                        selectedAcc.ConnectionStarted = AvitoConnection.EmptyDateTime;
                        await CloudTableUtils.InsertOrMergeEntityAsync(_tableClient, selectedAcc);

                        return await stepContext.BeginDialogAsync(nameof(AuthorizeDialog), default, cancellationToken);
                        break;
                    case "n:AccDisconnect":
                    case "Отключить":
                        selectedAcc.ConnectionStatus = (int)ConnectionStatusCodes.DeactivatedByUser;
                        selectedAcc.ConnectionErrorMessage = "Отключено пользователем";
                        selectedAcc.ConnectionErrorDate = DateTime.UtcNow;
                        await CloudTableUtils.InsertOrMergeEntityAsync(_tableClient, selectedAcc);
                        break;
                }


                await stepContext.Context.SendActivityAsync("Готово, настройки будут применены в течении минуты", cancellationToken: cancellationToken);
            }

            return await stepContext.EndDialogAsync(null, cancellationToken);
        }
    }
}
