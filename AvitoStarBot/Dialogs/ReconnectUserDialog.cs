﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Sellavi;
using AvitoStarBot.Extensions;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;

namespace AvitoStarBot.Dialogs
{
    public class ReconnectUserDialog : ComponentDialog
    {
        private readonly CloudBlobClient _cloudBlobClient;
        private readonly IConfiguration _iconfig;

        public ReconnectUserDialog(CloudBlobClient cloudBlobClient, IConfiguration iconfig) : base(nameof(ReconnectUserDialog))
        {
            _cloudBlobClient = cloudBlobClient;
            _iconfig = iconfig;

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginDialogAsync,
                EndDialogAsync
            }));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> BeginDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var botUserGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToMGuid();
            CloudBlobContainer cloudBlobContainer = _cloudBlobClient.GetContainerReference(_iconfig.GetSection("AppSettings:StorageAuthBlobName").Value);
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(botUserGuid + ".json");
            var blobContent = await cloudBlockBlob.DownloadTextAsync();
            var users = JsonConvert.DeserializeObject<List<SellaviUser>>(blobContent);

            var clientListChoices = new List<CardAction>();
            foreach (var user in users)
            {
                var row = user.UserName;
                clientListChoices.Add(new CardAction(ActionTypes.ImBack, row, value: user.UserName));
            }

            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Укажите аккаунт для переподключения") }, cancellationToken);
        }

        private async Task<DialogTurnResult> EndDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = (bool)stepContext.Result;
            return await stepContext.EndDialogAsync(result, cancellationToken: cancellationToken);
        }
    }
}
