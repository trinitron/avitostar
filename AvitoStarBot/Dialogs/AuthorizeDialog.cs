﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using Microsoft.Bot.Builder.Dialogs;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using AvitoModels.Hub;
using AvitoModels.Sellavi;
using AvitoStarBot.Extensions;
using Microsoft.Bot.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AvitoStarBot.Dialogs
{
    public class AuthorizeDialog : ComponentDialog
    {
        CloudQueueClient _queueClient;
        IConfiguration _iconfig;
        ILogger<RootDialog> _logger;
        private static HashSet<HubMessageUser> _preQueue;
        public AuthorizeDialog(CloudQueueClient queueClient, IConfiguration iconfig, ILogger<RootDialog> logger) : base(nameof(AuthorizeDialog))
        {
            _logger = logger;
            _queueClient = queueClient;
            _iconfig = iconfig;
            //TODO: проверить что очередь должна быть общая на все приложение;
            _preQueue = new HashSet<HubMessageUser>(new HubMessageUserComparer());

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                //BeginFormflowAsync,
                UserNameStepAsync,
                UserPassStepAsync,
                SendUserDataStepAsync,
                ResponseAwaiting,
                VerificationStepAsync,
                SaveResultAsync,
            }));

            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }


        private static async Task<DialogTurnResult> UserNameStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            await stepContext.Context.SendActivityAsync("Сейчас мы подключим вас к системе обмена сообщениями", cancellationToken: cancellationToken);
            await Task.Delay(TimeSpan.FromMilliseconds(2000), cancellationToken);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Укажите вашу почту либо телефон от кабинета Авито (88124256996)") }, cancellationToken);
        }

        private async Task<DialogTurnResult> UserPassStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["UserName"] = (string)stepContext.Result;
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Укажите пароль для входа") }, cancellationToken);
        }


        private async Task<DialogTurnResult> SendUserDataStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["UserPassword"] = (string)stepContext.Result;
            
            // Отправка пользователя на авторизацию
            var mGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToMGuid();
            await EnqueueAuthMessage(mGuid, stepContext);
            await HttpNotifier.RunWebJob(_iconfig);

            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Ожидайте, идет подключение...") }, cancellationToken);
            return new DialogTurnResult(DialogTurnStatus.Waiting);

        }

        private async Task<DialogTurnResult> ResponseAwaiting(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (stepContext.Options is AuthObject)
            {
                return await stepContext.NextAsync(stepContext.Options, cancellationToken);
            }

            if (stepContext.Result is bool && (bool)stepContext.Result)
            {
                //await stepContext.CancelAllDialogsAsync(cancellationToken);
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }

            if (stepContext.Result is bool && (bool)stepContext.Result == false)
            {
                return new DialogTurnResult(DialogTurnStatus.Waiting);
            }

            //loop this dialog because we are waiting for Notification from Authorization Server
            stepContext.ActiveDialog.State["stepIndex"] = (int)stepContext.ActiveDialog.State["stepIndex"] - 1;
            return await stepContext.PromptAsync(nameof(ConfirmPrompt), new PromptOptions { Prompt = MessageFactory.Text("Отменить добавление нового пользователя?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> VerificationStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var ao = (AuthObject)stepContext.Options; 
            if (ao.status.Contains("check"))
            {
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Требуется ввести код Авито (5 цифр)") }, cancellationToken);
            }
            if (ao.status.Contains("ok"))
            {
                return await stepContext.NextAsync(null, cancellationToken);
            }

            await stepContext.Context.SendActivityAsync(ao.GetMessage(), cancellationToken: cancellationToken);
            return await stepContext.EndDialogAsync(null, cancellationToken);

        }


        private async Task<DialogTurnResult> SaveResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!String.IsNullOrEmpty((string) stepContext.Result))
            {
                stepContext.Values["VerificationCode"] = (string)stepContext.Result;
                var mGuid = MD5.Calculate(stepContext.Context.Activity.From.Id).ToMGuid();
                await EnqueueAuthMessage(mGuid, stepContext);
                await HttpNotifier.RunWebJob(_iconfig);
            }

            await stepContext.Context.SendActivityAsync("Готово! Скоро вы начнете получать уведомления о входящих сообщениях", cancellationToken: cancellationToken);
            return await stepContext.EndDialogAsync(null, cancellationToken);
        }


        /// <summary>
        /// Постановка пользователя в очередь на авторизацию
        /// </summary>
        /// <param name="mGuid"></param>
        /// <param name="stepContext"></param>
        /// <returns></returns>
        private async Task EnqueueAuthMessage(string mGuid, WaterfallStepContext stepContext)
        {
            var u = new SellaviUser() { UserName = (string)stepContext.Values["UserName"], UserPassword = (string)stepContext.Values["UserPassword"] };
            stepContext.Values.TryGetValue("VerificationCode", out var code);
            var hmu = new HubMessageUser(u);
            hmu.VerificationCode = (string) code;
            hmu.MessageCreator = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            hmu.MessageOwnerGuid = mGuid;
            hmu.MessageCallbackUrl = new Uri(_iconfig.GetSection("AppSettings:BotNotificationAuth").Value + "/" + mGuid.ToGuid());


            _preQueue.RemoveWhere(x => x.AvitoUser.FormattedUserName == u.FormattedUserName && x.MessageDate < hmu.MessageDate.AddMinutes(-2));
            if (!_preQueue.Contains(hmu))
            {
                CloudQueue queue = _queueClient.GetQueueReference(_iconfig.GetSection("AppSettings:StorageAuthQueueName").Value);

                var jmessage = JsonConvert.SerializeObject(hmu);
                await queue.AddMessageAsync(new CloudQueueMessage(jmessage));

                _logger.LogInformation($"AuthorizeDialog.EnqueueAuthMessage: {jmessage}");
                _preQueue.Add(hmu);
            }
            else
            {
                var notificationMessage = new AuthObject("Уже идет регистрация такого пользователя, попробуйте повторно через 2 минуты");
                await HttpNotifier.NotifyClient(hmu.MessageCallbackUrl, JsonConvert.SerializeObject(notificationMessage));
            }
        }
    }
}
