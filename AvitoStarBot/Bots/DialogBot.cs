﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.TableEntities;
using AvitoStarBot.Extensions;
using AvitoStarBot.Interfaces;
using CloudTableExtensions;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace AvitoStarBot.Bots
{
    // This IBot implementation can run any type of Dialog. The use of type parameterization is to 
    // allow multiple different bots to be run at different endpoints within the same project. This
    // can be achieved by defining distinct Controller typesk, each with dependency on distinct IBot
    // types, this way ASP Dependency Injection can glue everything together without ambiguity.
    // The ConversationState is used by the Dialog system. The UserState isn't, however, it might
    // have been used in a Dialog implementation, and the requirement is that all BotState objects
    // are saved at the end of a turn.
    /*
     * Добавление бота в друзья сопровождается следующими событиями (для WebChat, Bot Emulator)
     * Microsoft.Bot.Builder.ActivityHandler: Event fired - DialogBot.OnTurnAsync:
     * Microsoft.Bot.Builder.ActivityHandler: Event fired - DialogBot.OnConversationUpdateActivityAsync:
     * Microsoft.Bot.Builder.ActivityHandler: Event fired - DialogBot.OnMembersAddedAsync:
     *
     * Однако, (для Telegram) последовательностью событий такая-же как при приеме обычного сообщения
     * Microsoft.Bot.Builder.ActivityHandler: Event fired - DialogBot.OnTurnAsync:
     * Microsoft.Bot.Builder.ActivityHandler: Event fired - DialogBot.OnMessageActivityAsync:
     */
    public class DialogBot<T> : ActivityHandler where T : Dialog
    {
        protected readonly Dialog _dialog;
        protected readonly BotState _conversationState;
        protected readonly CloudTableClient _tableClient;
        protected readonly ILogger _logger;

        // Dependency injected dictionary for storing ConversationReference objects used in NotifyController to proactively message users
        private ConcurrentDictionary<string, ConversationReference> _conversationReferences;

        public DialogBot(ConversationState conversationState, T dialog, ConcurrentDictionary<string, ConversationReference> conversationReferences, CloudTableClient tableClient, ILogger<ActivityHandler> logger)
        {
            _conversationReferences = conversationReferences;
            _conversationState = conversationState;
            _dialog = dialog;
            _tableClient = tableClient;
            _logger = logger;
        }

        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnTurnAsync");
            _logger.LogDebug(JsonConvert.SerializeObject(turnContext.Activity));

            await base.OnTurnAsync(turnContext, cancellationToken);
            // Save any state changes that might have occured during the turn.
            await _conversationState.SaveChangesAsync(turnContext, false, cancellationToken);
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnMessageActivityAsync");
            // !!
            AddConversationReference(turnContext.Activity as Activity);
            // Run the Dialog with the new message Activity.
            await _dialog.RunAsync(turnContext, _conversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);

        }

        /// <summary>
        /// Добавление ссылки на диалог в общий контейнетр conversationReferences для доступа из NotifyController
        /// </summary>
        /// <param name="activity"></param>
        private async void AddConversationReference(Activity activity)
        {
            var conversationReference = activity.GetConversationReference();
            var userGuid = MD5.Calculate(conversationReference.User.Id).ToGuid().ToString();

            // Сохранение в постоянную память ConversationReference
            if (!_conversationReferences.ContainsKey(userGuid) && !conversationReference.ServiceUrl.Contains("localhost"))
            {
                var entity = new AvitoBotReference("P1", userGuid) { ConversationReference = JsonConvert.SerializeObject(conversationReference) };
                await CloudTableUtils.InsertOrMergeEntityAsync<AvitoBotReference>(_tableClient, entity);
            }

            _conversationReferences.AddOrUpdate(userGuid, conversationReference, (key, newValue) => conversationReference);
        }

        protected override Task OnConversationUpdateActivityAsync(ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnConversationUpdateActivityAsync");
            AddConversationReference(turnContext.Activity as Activity);

            return base.OnConversationUpdateActivityAsync(turnContext, cancellationToken);
        }


        protected override async Task OnEventActivityAsync(ITurnContext<IEventActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnEventActivityAsync");
            var newMembers = JsonConvert.SerializeObject(turnContext);
            _logger.LogInformation(newMembers);
            if (turnContext.Activity.Name == "webchat/join")
            {
                await turnContext.SendActivityAsync("Welcome Message!");
            }
        }

        // Greet when users are added to the conversation.
        // Note that all channels do not send the conversation update activity.
        // If you find that this bot works in the emulator, but does not in
        // another channel the reason is most likely that the channel does not
        // send this activity.
        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnMembersAddedAsync");
            var newMembers = JsonConvert.SerializeObject(membersAdded);
            _logger.LogInformation(newMembers);

            foreach (var member in membersAdded)
            {
                //if (member.Id != turnContext.Activity.Recipient.Id)
                if (member.Id != turnContext.Activity.Recipient.Id || member.Name == "User")
                {
                    AddConversationReference(turnContext.Activity as Activity);

                    // Приветствие пользователя (однократное при добавлении бота)
                    // (работает для Bot Framework Emulator, но для Telegram используется команда /start)
                    await ((IWelcomableDialog)_dialog).SendWelcomeCardAsync(turnContext, cancellationToken);

                    // Запуск RootDialog
                    await _dialog.RunAsync(turnContext, _conversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);
                }
            }
        }

        protected override Task OnMembersRemovedAsync(IList<ChannelAccount> membersRemoved, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnMembersRemovedAsync");
            return base.OnMembersRemovedAsync(membersRemoved, turnContext, cancellationToken);
        }

        protected override Task OnTokenResponseEventAsync(ITurnContext<IEventActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnTokenResponseEventAsync");
            return base.OnTokenResponseEventAsync(turnContext, cancellationToken);
        }

        protected override Task OnEventAsync(ITurnContext<IEventActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnEventAsync");
            return base.OnEventAsync(turnContext, cancellationToken);

        }
        protected override Task OnUnrecognizedActivityTypeAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Event fired - DialogBot.OnUnrecognizedActivityTypeAsync");
            return base.OnUnrecognizedActivityTypeAsync(turnContext, cancellationToken);
        }




    }
}
