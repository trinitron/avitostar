﻿using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AvitoModels.Hub;
using Newtonsoft.Json;

namespace AvitoStarBot.Extensions
{
    public class HttpNotifier
    {
        /// <summary>
        /// Запуск WebJob для старта обработки очереди / блобов на авторизацию
        /// </summary>
        /// <returns></returns>
        public static async Task RunWebJob(IConfiguration config)
        {
            try
            {
                Trace.TraceInformation("Запуск WebJob для обработки очереди");
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Authorization",
                    "Basic " + config.GetSection("WebJobKey").Value);
                var response = await httpClient.PostAsync(config.GetSection("WebJobUrl").Value, null);
                //var content = await response.Content.ReadAsStringAsync();
                Trace.TraceInformation("Запуск webjob завершен с кодом: {0}", response.StatusCode);
            }
            catch (WebException ex)
            {
                var webResponse = ex.Response as System.Net.HttpWebResponse;
                Trace.TraceError("Статус запуска RunWebJob: {0}", webResponse?.StatusCode);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Исключение во время запуска RunWebJob: {0}", ex);
            }
        }

        public static async Task NotifyClient(HubMessage message)
        {
            try
            {
                var httpClient = new HttpClient();
                var request = message.MessageCallbackUrl + "/" + message.MessageOwnerGuid.ToGuid().ToString();

                Trace.TraceInformation("Отправка запроса: {0}", request);
                //Trace.TraceInformation("Содержимое запроса: {0}", jMessage);
                var jMessage = JsonConvert.SerializeObject(message);

                var stringContent = new StringContent(jMessage, Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(request, stringContent);
                var bodyContent = await response.Content.ReadAsStringAsync();
                Trace.TraceInformation("Статус отправки {0},  содержимое: {1}", response.StatusCode,
                    (String.IsNullOrEmpty(bodyContent) ? "-" : bodyContent));
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception in AvitoStarBot.Extensions.HttpNotifier.NotifyClient: {0}", ex);
            }
        }

        public static async Task NotifyClient(Uri callBackUrl, string message)
        {
            try
            {
                var httpClient = new HttpClient();
                Trace.TraceInformation("AvitoStarBot.Extensions.HttpNotifier.NotifyClient: {0}", callBackUrl.AbsoluteUri);

                var stringContent = new StringContent(message, Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(callBackUrl, stringContent);
                var bodyContent = await response.Content.ReadAsStringAsync();
                Trace.TraceInformation("NotifyClient.SendStatus: {0}", response.StatusCode);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception in AvitoStarBot.Extensions.HttpNotifier.NotifyClient: {0}", ex);
            }
        }
    }
}
