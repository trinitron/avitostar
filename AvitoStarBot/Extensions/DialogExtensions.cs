﻿using System;
using System.Threading.Tasks;
using AvitoModels.Sockets;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace AvitoStarBot.Extensions
{
    public static class DialogExtensions
    {
        public static DialogInstance GetUnderlyingDialogInstance(this DialogContext dialogContext, string dialogId)
        {
            try
            {
                if (dialogContext == null)
                    return null;
                if (dialogContext.ActiveDialog?.Id == dialogId)
                    return dialogContext.ActiveDialog;

                var childContext = dialogContext.Child;
                if (childContext != null)
                {
                    if (childContext.ActiveDialog?.Id == dialogId)
                        return childContext.ActiveDialog;

                    GetUnderlyingDialogInstance(childContext, dialogId);
                }
            }
            catch (Exception)
            {
                // Для некоторых диалогов невозможно получить Child диалог из-за:
                // System.ObjectDisposedException: Cannot access a disposed object.Object name: 'Get'.
                return null;
            }
            return null;

        }

        /// <summary>
        /// Постановка сообщения в очередь на отправку в Авито
        /// </summary>
        /// <param name="telegramText">Текст сообщения</param>
        /// <param name="channelId">Идентификатор чата (получателя)</param>
        /// <param name="userId">Идентификатор отправителя</param>
        /// <returns></returns>
        public static async Task EnqueueAvitoMessage(this CloudQueueClient queueClient, string telegramText, string channelId, int userId)
        {
            CloudQueue queue = queueClient.GetQueueReference("avito-message-queue");
            var message = new AvitoMessageOut();
            message.@params.text = telegramText;
            message.@params.channelId = channelId;
            message.userId = userId.ToString();
            var jmessage = JsonConvert.SerializeObject(message);



#if !DEBUG
            await queue.AddMessageAsync(new CloudQueueMessage(jmessage.ToString()));
            
            //Lucky Number S7evin
            if (new Random().Next(1, 20) == 7)
            {
                message.@params.text = "Это сообщение отправлено при помощи @avitostar_bot";
                jmessage = JsonConvert.SerializeObject(message);
                await queue.AddMessageAsync(new CloudQueueMessage(jmessage.ToString()));
            }
#endif
        }
    }
}
