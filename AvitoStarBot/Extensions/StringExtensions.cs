﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AvitoStarBot.Extensions
{
    public static class StringExtensions
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static IEnumerable<TResult> CastNullable<TResult>(this IEnumerable source)
        {
            IEnumerable<TResult> results = source as IEnumerable<TResult>;
            if (results != null)
                return results;
            if (source == null)
                throw (Exception)new ArgumentNullException(nameof(source));
            return CastIterator<TResult>(source);
        }

        private static IEnumerable<TResult> CastIterator<TResult>(IEnumerable source)
        {
            var res = new Collection<TResult>();
            foreach (var result in source)
            {
                if (result != null && result.GetType().IsValueType)
                {
                    res.Add((TResult)((object)result.ToString()));
                }
                else
                {
                    res.Add((TResult)result);
                }

            }
            return res;
        }

        public static dynamic CastFromString(this string source, Type newType)
        {
            if (newType.IsGenericType && newType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (String.IsNullOrEmpty(source))
                    return null;
                
                var underlyingType = Nullable.GetUnderlyingType(newType) ?? newType;
                dynamic c2 = Convert.ChangeType(source, underlyingType);
                return c2;
            }

            return source;
        }

        public static T? GetValueOrNull<T>(this string valueAsString)
            where T : struct
        {
            if (string.IsNullOrEmpty(valueAsString))
                return null;
            return (T)Convert.ChangeType(valueAsString, typeof(T));
        }


    }
}
