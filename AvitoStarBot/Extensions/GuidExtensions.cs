﻿using System;

namespace AvitoStarBot.Extensions
{
    public static class GuidExtensions
    {
        public static Guid ToGuid(this string input)
        {
            return new Guid(input.Replace("-", ""));
        }

        public static string ToMGuid(this string input)
        {
            var s = input.ToString().Replace("-", "");
            s = s.Insert(4, "-");
            s = s.Insert(9, "-");
            s = s.Insert(14, "-");
            s = s.Insert(19, "-");
            s = s.Insert(24, "-");
            s = s.Insert(29, "-");
            s = s.Insert(34, "-");
            return s.ToUpper();
        }

    }
}
