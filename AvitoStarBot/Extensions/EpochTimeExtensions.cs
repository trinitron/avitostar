﻿using System;

namespace AvitoStarBot.Extensions
{
    public static class EpochTimeExtensions
    {
        /// <summary>
        /// Converts the given date value to epoch time.
        /// </summary>
        public static long ToEpochTime(this DateTime dateTime)
        {
            var date = dateTime.ToUniversalTime();
            var ticks = date.Ticks - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).Ticks;
            var ts = ticks / TimeSpan.TicksPerSecond;
            return ts;
        }

        /// <summary>
        /// Converts the given date value to epoch time.
        /// </summary>
        public static long ToEpochTime(this DateTimeOffset dateTime)
        {
            var date = dateTime.ToUniversalTime();
            var ticks = date.Ticks - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).Ticks;
            var ts = ticks / TimeSpan.TicksPerSecond;
            return ts;
        }

        /// <summary>
        /// Converts the given epoch time to a <see cref="DateTime"/> with <see cref="DateTimeKind.Utc"/> kind.
        /// </summary>
        public static DateTime ToDateTimeFromEpoch(this long intDate)
        {
            var timeInTicks = intDate * TimeSpan.TicksPerSecond;
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddTicks(timeInTicks);
        }

        /// <summary>
        /// Converts the given epoch time to a UTC <see cref="DateTimeOffset"/>.
        /// </summary>
        public static DateTimeOffset ToDateTimeOffsetFromEpoch(this long intDate)
        {
            var timeInTicks = intDate * TimeSpan.TicksPerSecond;
            return new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).AddTicks(timeInTicks);
        }

        /// <summary>
        /// Преобразование TimeStamp Avito к строковому формату
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <param name="dateFormat"></param>
        /// <param name="timeZoneName"></param>
        /// <returns></returns>
        public static string TryToConvertDate(this long timeStamp, string dateFormat = "HH:mm", string timeZoneName = "Russian Standard Time")
        {
            try
            {
                var shortStamp = timeStamp.ToString().Substring(0, 10);
                var mDate = Convert.ToInt64(shortStamp).ToDateTimeFromEpoch();
                var moscowTime = TimeZoneInfo.ConvertTime(mDate, TimeZoneInfo.FindSystemTimeZoneById(timeZoneName));
                var tDate = moscowTime.ToString(dateFormat);
                return tDate;
            }
            catch (Exception ex)
            {
                return @"--:--";
            }
        }
    }
}
