// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using AvitoStarBot.Bots;
using AvitoStarBot.Dialogs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.BotFramework;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Connector.Authentication;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;

namespace AvitoStarBot
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add the whole configuration object here.
            services.AddSingleton<IConfiguration>(Configuration);

            // Create the credential provider to be used with the Bot Framework Adapter.
            services.AddSingleton<ICredentialProvider, ConfigurationCredentialProvider>();

            // Create the Bot Framework Adapter with error handling enabled. 
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            // Create the storage we'll be using for User and Conversation state. (Memory is great for testing purposes.) 
            services.AddSingleton<IStorage, MemoryStorage>();


            // Create the Conversation state. (Used by the Dialog system itself.)
            services.AddSingleton<ConversationState>();

            // Create the Root Dialog as a singleton
            services.AddSingleton<RootDialog>();

            //TODO: Switch project to core 2.2 / 3.1 to Load Assemblies Dynamically
            //https://anthonygiretti.com/2019/01/31/implement-dynamic-dependency-injection-in-asp-net-core-2-2-example-with-multitenancy-scenario/
            //http://codebuckets.com/2020/05/29/dynamically-loading-assemblies-for-dependency-injection-in-net-core/
            services.AddSingleton<AccountSettingsDialog>();
            services.AddSingleton<AuthorizeDialog>();
            services.AddSingleton<CancelDialog>();
            services.AddSingleton<ClientListDialog>();
            services.AddSingleton<LoopUserDialog>();
            services.AddSingleton<ReplyUserDialog>();
            services.AddSingleton<NewMessageDialog>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            services.AddTransient<IBot, DialogBot<RootDialog>>();

            // Create a global hashset for our ConversationReferences (shared with NotifyController)
            services.AddSingleton<ConcurrentDictionary<string, ConversationReference>>();

            // Create the User state.
            services.AddSingleton<UserState>();

            // logging
            services.AddLogging((logging) =>
            {
                logging.AddConfiguration(Configuration.GetSection("Logging"));
                logging.AddConsole();
            });

            //services.Configure Messages Queues
            services.AddSingleton(provider =>
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Configuration.GetConnectionString("AzureStorageConnectionString-1"));
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
                return queueClient;
            });

            services.AddSingleton<CloudTableClient>(ctx =>
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Configuration.GetConnectionString("AzureStorageConnectionString-1"));
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                return tableClient;
            });
            services.AddSingleton<CloudBlobClient>(ctx =>
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Configuration.GetConnectionString("AzureStorageConnectionString-1"));
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return cloudBlobClient;
            });

            //Singleton - instance that will last the entire lifetime of the application
            //Transient - each time the service is requested, a new instance is created
            //Scoped    - one instance per web request
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            //app.UseHttpsRedirection();
            app.UseMvc();
        }

        public static IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}
