﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Hub;
using AvitoModels.Sellavi;
using AvitoModels.Sockets;
using AvitoModels.TableEntities;
using AvitoStarBot.Dialogs;
using AvitoStarBot.Extensions;
using CloudTableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using ChannelAccount = Microsoft.Bot.Schema.ChannelAccount;

namespace AvitoStarBot.Controllers
{
    [Route("api/notify")]
    [ApiController]
    public class NotifyController : ControllerBase
    {
        private readonly IBotFrameworkHttpAdapter _adapter;
        private readonly string _appId;
        private readonly IStorage _iStorage;
        private readonly RootDialog _rootDialod;
        private readonly ConversationState _conversationState;
        private readonly CloudTableClient _tableClient;
        private readonly ConcurrentDictionary<string, ConversationReference> _conversationReferences;
        private readonly ILogger<NotifyController> _logger;
        private const int IMML = 10; // InMemoryMessageLimit


        public NotifyController(IBotFrameworkHttpAdapter adapter, IConfiguration configuration,
            ConcurrentDictionary<string, ConversationReference> conversationReferences,
            ConversationState conversationState, IStorage iStorage, RootDialog rootDialod, CloudTableClient tableClient,
            ILogger<NotifyController> logger)
        {
            _adapter = adapter;
            _rootDialod = rootDialod;
            _iStorage = iStorage;
            _tableClient = tableClient;
            _conversationReferences = conversationReferences;
            _conversationState = conversationState;
            _logger = logger;

            _appId = configuration["MicrosoftAppId"];

            // If the channel is the Emulator, and authentication is not in use,
            // the AppId will be null.  We generate a random AppId for this case only.
            // This is not required for production, since the AppId will have a value.
            if (string.IsNullOrEmpty(_appId))
            {
                _appId = Guid.NewGuid().ToString(); //if no AppId, use a random Guid
            }
        }

        [HttpPost]
        [Route("avito/{clientId}")]
        public async Task<IActionResult> NotifyClientAvitoMessage(Guid clientId, [FromBody] AvitoMessageIn messageText)
        {
            _logger.LogInformation("NotifyController.NotifyClientAvitoMessage: {0}", JsonConvert.SerializeObject(messageText));

            var cr = await FindReference(clientId);
            _logger.LogInformation("NotifyController.ConversationReference: {0}", JsonConvert.SerializeObject(cr));

            await ((BotAdapter)_adapter).ContinueConversationAsync(_appId, cr, async (context, token) => await BotCallback(messageText, context, token), default(CancellationToken));
            return new ContentResult()
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
            };
        }

        [HttpPost]
        [Route("dialog/{clientId}")]
        public async Task<IActionResult> NotifyClientDialog(Guid clientId, [FromBody] HubMessageDialog message)
        {
            _logger.LogInformation("NotifyController.NotifyClientDialog: {0}", JsonConvert.SerializeObject(message));
            var cr = await FindReference(clientId);

            await ((BotAdapter)_adapter).ContinueConversationAsync(_appId, cr, async (turnContext, token) =>
            {
                if (_rootDialod.Dialogs.Find(message.Dialog) != null)
                {
                    await _rootDialod.RunAsync(turnContext, _conversationState.CreateProperty<DialogState>(nameof(DialogState)), token);
                    await _rootDialod.DialogContext.ReplaceDialogAsync(message.Dialog, message.Options, token);
                    await _conversationState.SaveChangesAsync(turnContext, false, token);
                }
                else
                {
                    await turnContext.SendActivityAsync(message.Dialog, cancellationToken: token);
                }
            }, default(CancellationToken));
            return new ContentResult()
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
            };
        }

        [HttpPost]
        [Route("auth/{clientId}")]
        public async Task<IActionResult> NotifyClientAuth(Guid clientId, [FromBody] AuthObject aObject)
        {
            _logger.LogInformation("NotifyController.NotifyClientAuth: {0}", JsonConvert.SerializeObject(aObject));
            var cr = await FindReference(clientId);
            if (cr != null)
            {
                await ((BotAdapter)_adapter).ContinueConversationAsync(_appId, cr,
                    async (turnContext, token) =>
                    {
                        if (_rootDialod.DialogContext != null && aObject.status.Contains("check"))
                        {
                            // Есть возможность получить состояния диалога через _conversationState
                            // var dialogState = (DialogState)_conversationState.GetCachedState(rootTurnContext).State["DialogState"];
                            // var dialogState = (DialogState)_rootDialod.DialogContext.ActiveDialog.State["dialogs"];

                            // Смена WaterFallStep (переход дальше по диалогу)
                            // int.TryParse(dialogState.DialogStack.LastOrDefault().State["stepIndex"].ToString(), out var waterFallStep);
                            // dialogState.DialogStack.LastOrDefault().State["stepIndex"] = ++waterFallStep;

                            try
                            {
                                var rootTurnContext = _rootDialod.DialogContext.Context;
                                var authorizeDialog = _rootDialod.DialogContext.GetUnderlyingDialogInstance(nameof(AuthorizeDialog));
                                var dialogState = (DialogState)authorizeDialog.State["dialogs"];
                                dialogState.DialogStack.LastOrDefault().State["options"] = aObject;


                                // возобновление диалога AuthorizeDialog и сохранение состояния измененного stepIndex 
                                await _rootDialod.DialogContext.ContinueDialogAsync(token);
                                await _conversationState.SaveChangesAsync(rootTurnContext, false, token);
                            }
                            catch (Exception)
                            {
                                var dialogstack = _rootDialod.DialogContext?.Stack.Select(x => x.Id.ToString()).Aggregate((a, b) => a + " <= " + b);
                                _logger.LogError("NotifyController.NotifyClientAuth.ErrorStack: {0}", dialogstack);
                                throw;
                            }
                        }
                        else
                        {
                            if (_rootDialod.DialogContext != null) await _rootDialod.DialogContext.CancelAllDialogsAsync(token);
                            await _conversationState.DeleteAsync(turnContext, token);
                            var message = aObject.status == "ok" ? "✔️ Аккаунт " + aObject.result.user.ProfileId + " подключен!" : "⚠️ " + aObject.GetMessage();
                            await turnContext.SendActivityAsync(message, cancellationToken: token);
                        }
                    },
                    default(CancellationToken));
                return new ContentResult()
                {
                    ContentType = "text/html",
                    StatusCode = (int)HttpStatusCode.OK,
                };
            }
            else
            {
                _logger.LogWarning($"Conversation reference not found, user: {clientId.ToString()} cannot be notified!");
                return new ContentResult()
                {
                    ContentType = "text/html",
                    StatusCode = (int)HttpStatusCode.NotFound,
                };
            }
        }

        /// <summary>
        /// Метод, через который будет происходить уведомление клиента о новых сообщениях из Авито
        /// </summary>
        /// <param name="turnContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task BotCallback(AvitoMessageIn jMessage, ITurnContext turnContext, CancellationToken cancellationToken)
        {
            try
            {
                // If you encounter permission-related errors when sending this message, see
                // https://aka.ms/BotTrustServiceUrl

                // Сохранение сообщения в память (TODO: делать это лучше через userState)
                var botUserGuid = MD5.Calculate(turnContext.Activity.From.Id).ToGuid().ToString();
                var memoryItem = await _iStorage.ReadAsync(new[] { botUserGuid }, cancellationToken);
                var userMessages = memoryItem.ContainsKey(botUserGuid) ? (memoryItem[botUserGuid] as List<AvitoMessageIn>) : new List<AvitoMessageIn>();

                var isNewDialogNeeded = (userMessages.LastOrDefault()?.value.fromUid != jMessage.value.fromUid) ||
                                        (_rootDialod.DialogContext.GetUnderlyingDialogInstance(nameof(LoopUserDialog)) == null);
                if (userMessages.Count > IMML)
                    userMessages.RemoveAt(IMML);
                userMessages.Add(jMessage);

                memoryItem = new Dictionary<string, object>() { { botUserGuid, userMessages } };
                await _iStorage.WriteAsync(memoryItem, cancellationToken);

                if (isNewDialogNeeded)
                {
                    // https://docs.microsoft.com/en-us/azure/bot-service/bot-builder-howto-v4-state?view=azure-bot-service-4.0&tabs=csharp
                    var conversationStateAccessors = _conversationState.CreateProperty<DialogState>(nameof(DialogState));
                    var conversationData = await conversationStateAccessors.GetAsync(turnContext, () => new DialogState(), cancellationToken);

                    var rootDialogStack = conversationData.DialogStack.ToList();

                    // Если Root диалог пустой (или не запускался пользователем, например после перезагрузки сервера)
                    if (rootDialogStack.Count == 0)
                    {
                        var rootDialogState = new DialogState();
                        var rootDialogInstance = new DialogInstance() { Id = nameof(RootDialog), State = new Dictionary<string, object>() { { "dialogs", rootDialogState } } };
                        rootDialogStack = new List<DialogInstance>() { rootDialogInstance };
                    }

                    // Создание диалога-уведомления о новом входящем сообщении
                    DialogSet dialogSet = new DialogSet(conversationStateAccessors);
                    dialogSet.Add(_rootDialod.FindDialog(nameof(NewMessageDialog)));
                    var dc = await dialogSet.CreateContextAsync(turnContext, cancellationToken);
                    await dc.BeginDialogAsync(nameof(NewMessageDialog), jMessage, cancellationToken);

                    // Замена DialogStack главного диалога на стек NewMessageDialog
                    var newDialogInstance = conversationData.DialogStack.ToList().FirstOrDefault();
                    var dialogState = (DialogState)rootDialogStack[0].State["dialogs"];

                    dialogState.DialogStack = new List<DialogInstance>() { newDialogInstance }; //DialogStack.Add()?
                    conversationData.DialogStack = rootDialogStack;

                    // Сохранение conversationData
                    await _conversationState.SaveChangesAsync(turnContext, false, cancellationToken);
                }
                else
                {
                    var msg = String.Format("💬 *{0}*", jMessage.value.body.text);
                    await turnContext.SendActivityAsync(msg, cancellationToken: cancellationToken);
                    await _rootDialod.DialogContext.ReplaceDialogAsync(nameof(LoopUserDialog), jMessage, cancellationToken);
                }

                // Отменяем все предыдущие действия пользователя из Root диалога  
                //if (_rootDialod.DialogContext != null) await _rootDialod.DialogContext.CancelAllDialogsAsync(cancellationToken);
                //await _conversationState.DeleteAsync(turnContext, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"NotifyController.BotCallback.Exception: ");
            }

        }

        /// <summary>
        /// Helper function to deal with the persisted values we collect in this dialog.
        /// </summary>
        /// <param name="dialogInstance">A handle on the runtime instance associated with this dialog, the State is a property.</param>
        /// <returns>A dictionary representing the current state or a new dictionary if we have none.</returns>
        private static IDictionary<string, object> GetPersistedValues(DialogInstance dialogInstance)
        {
            object obj;
            if (!dialogInstance.State.TryGetValue("values", out obj))
            {
                obj = new Dictionary<string, object>();
                dialogInstance.State.Add("values", obj);
            }

            return (IDictionary<string, object>)obj;
        }

        //private async void UpdateUserChidReference(AvitoMessageIn jMessage, string userGuid)
        //{
        //    var entity = new AvitoUserChid(userGuid, jMessage.value.channelId);
        //    await CloudTableUtils.InsertOrMergeEntityAsync<AvitoUserChid>(_tableClient, entity);
        //}


        private async Task<ConversationReference> FindReference(Guid clientId)
        {
            // Пытаемся выяснить причину появления ошибки (убрать семафор если это не является критической секцией)
            // Microsoft.Bot.Schema.ErrorResponseException: Operation returned an invalid status code 'Forbidden'
            var semaphore = new SemaphoreSlim(1, 1);
            await semaphore.WaitAsync();
            try
            {
                if (!_conversationReferences.TryGetValue(clientId.ToString(), out var cr))
                {
                    var row = await CloudTableUtils.RetrieveEntityUsingPointQueryAsync<AvitoBotReference>(_tableClient, "P1", clientId.ToString());
                    if (row != null)
                    {
                        cr = JsonConvert.DeserializeObject<ConversationReference>(row.ConversationReference);
                        _conversationReferences.AddOrUpdate(clientId.ToString(), cr, (key, newValue) => cr);
                    }
                }

                return cr ?? throw new Exception("Conversational reference is not found for clientId: " + clientId.ToString()); ;
            }
            finally
            {
                semaphore.Release();
            }
        }


        [HttpGet("/api/notify/users")]
        public IActionResult ListUsers()
        {
            var users = new List<ChannelAccount>();
            foreach (var conversationReference in _conversationReferences.Values)
            {
                //((BotAdapter)_adapter).ContinueConversationAsync(_appId, conversationReference, BotCallback, default(CancellationToken));

                users.Add(conversationReference.User);
            }

            var jUsers = JsonConvert.SerializeObject(_conversationReferences /*users*/);
            // Let the caller know proactive messages have been sent
            return new ContentResult()
            {
                Content = jUsers,
                ContentType = "application/json",
                StatusCode = (int)HttpStatusCode.OK,
            };
        }




        /// <summary>
        /// Массовое уведомление пользователей (не используется)
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="messageText"></param>
        /// <returns></returns>
        public async Task<IActionResult> NotifyAllUsers(Guid clientId, [FromBody] AuthObject messageText)
        {
            var message = messageText.GetMessage();
            foreach (var conversationReference in _conversationReferences.Values.Where(x => MD5.Calculate(x.User.Id).ToGuid() == clientId))
            {
                await ((BotAdapter)_adapter).ContinueConversationAsync(_appId, conversationReference,
                    (context, token) => context.SendActivityAsync(message, cancellationToken: token),
                    default(CancellationToken));
            }

            return new ContentResult()
            {
                //Content = "<html><body><h1>Proactive messages have been sent.</h1></body></html>",
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
            };
        }
    }
}
