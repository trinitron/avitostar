﻿using System.IO;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace AvitoSocketApp
{
    class Program
    {
        /// <summary>
        /// https://mderriey.com/2018/08/02/autofac-integration-in-asp-net-core-generic-hosts/
        /// https://docs.microsoft.com/en-us/azure/app-service/webjobs-sdk-how-to
        /// https://autofaccn.readthedocs.io/en/latest/integration/aspnetcore.html
        /// </summary>
        /// <param name="args"></param>
        public static async Task Main(string[] args)
        {
            var builder = new HostBuilder();

            // registering Webjobs and Functions
            // BUG: WebJobsHostBuilderExtensions.ConfigureWebJobs should not add configuration sources #1931
            builder.ConfigureWebJobs(b =>
            {
                b.AddAzureStorageCoreServices();
                b.AddAzureStorage(a =>
                {
                    a.BatchSize = 8;
                    a.NewBatchThreshold = 4;
                    a.MaxDequeueCount = 4;
                });
                b.AddTimers();

            });

            // host config
            builder.ConfigureHostConfiguration(configHost =>
            {
                configHost.SetBasePath(Directory.GetCurrentDirectory());
                configHost.AddJsonFile("hostsettings.json", optional: true);
                configHost.AddEnvironmentVariables(prefix: "PREFIX_");
                configHost.AddCommandLine(args);
            });

            // app config
            builder.ConfigureAppConfiguration((hostContext, config) =>
             {
                 config.Sources.Clear(); //because it was created in ConfigureWebJobs;
                 config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                 config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                 config.AddEnvironmentVariables(prefix: "PREFIX_");
                 config.AddCommandLine(args);
             });

            // registering BackgroundService for Application (WebSocket4Net)
            builder.ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<Application>();
                services.AddSingleton<CloudTableClient>(ctx =>
                {
                    var connectionString = hostContext.Configuration.GetSection("ConnectionStrings:AzureWebJobsStorage");
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString.Value);
                    CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                    return tableClient;
                });
            });

            // logging
            builder.ConfigureLogging((hostingContext, logging) =>
            {
                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                logging.AddConsole();
            });

            // DI Container
            builder.UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureContainer<ContainerBuilder>(bld =>
                {
                    // registering services in the Autofac ContainerBuilder
                    bld.RegisterType<Application>().As<IApplication>();
                    bld.RegisterType<AvitoSocketFactory>().As<IAvitoSocketFactory>();
                    //bld.RegisterType<SingleSocket>().As<ISingleSocket>();

                });

            var host = builder.UseConsoleLifetime().Build();
            await host.RunAsync();
        }
    }
}