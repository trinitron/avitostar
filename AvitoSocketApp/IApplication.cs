﻿namespace AvitoSocketApp
{
    public interface IApplication
    {
        void Run();
    }
}