﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace AvitoSocketApp
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-2.1#host-configuration
    /// </summary>
    public class Application : BackgroundService, IApplication
    {
        private readonly IAvitoSocketFactory _avtitoSocketFactory;
        private readonly IApplicationLifetime _appLifetime;

        public Application(IAvitoSocketFactory avtitoSocketFactory, IApplicationLifetime appLifetime)
        {
            _avtitoSocketFactory = avtitoSocketFactory;
            _appLifetime = appLifetime;
        }

        public void Run()
        {
            _avtitoSocketFactory.PopulateConnectionPool();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _appLifetime.ApplicationStopping.Register(OnStopped);
            return Task.Run(() => Run(), stoppingToken);
        }

        //public override Task StartAsync(CancellationToken cancellationToken)
        //{
        //    _appLifetime.ApplicationStarted.Register(Run);
        //    _appLifetime.ApplicationStopping.Register(OnStopping);
        //    _appLifetime.ApplicationStopped.Register(OnStopped);

        //    return Task.CompletedTask;
        //}

        private void OnStopped()
        {
            // Perform on-stopping activities here
            //_avtitoSocketFactory.ShutDownAllConnections();
            Console.WriteLine("Window is closing...");
            _avtitoSocketFactory.ShutDownAllConnections();
            //Log.Information("Window will close automatically in 20 seconds.");
            //Task.Delay(20000).GetAwaiter().GetResult();
        }
    }
}
