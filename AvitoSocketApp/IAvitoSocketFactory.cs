﻿using System.Threading.Tasks;
using AvitoModels.TableEntities;

namespace AvitoSocketApp
{
    public interface IAvitoSocketFactory
    {
        Task<bool> CreateNewConnectionAsync(AvitoConnection connectionEntity);
        Task<SingleSocket> RetrieveFromPool(string chid);
        void PopulateConnectionPool();
        void RemoveFromPool(AvitoConnection connectionEntity);
        void CleanUpConnectionPool();
        void ShutDownAllConnections();
    }
}
