﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AvitoSocketApp
{
    public class HttpNotifier
    {
        /// <summary>
        /// Уведомление клиенту о новом входящем сообщении
        /// </summary>
        /// <param name="endpointUrl">Url серверной части бота</param>
        /// <param name="jMessage">Сообщение (json)</param>
        /// <returns></returns>
        public static async Task NotifyClient(string endpointUrl, string jMessage)
        {
            try
            {
                Trace.TraceInformation("Отправка сообщения клиенту: {0}", endpointUrl);
                Trace.TraceInformation("Содержимое запроса: {0}", jMessage);

                var httpClient = new HttpClient();
                var stringContent = new StringContent(jMessage, Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(endpointUrl, stringContent);
                var bodyContent = await response.Content.ReadAsStringAsync();

                Trace.TraceInformation("Статус отправки {0},  ответ: {1}", response.StatusCode,
                    (String.IsNullOrEmpty(bodyContent) ? "-" : bodyContent));
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception in AvitoSocketApp.HttpNotifier: {0}", ex);
            }
        }

        /// <summary>
        /// Выполнение действий с лицензией (получение / обновление)
        /// </summary>
        /// <param name="mGuid">Machine Guid (4x8)</param>
        /// <param name="jMessage">Json serialized array SellaviUser[]</param>
        /// <param name="action">0 - get license; 1 - update license information;</param>
        /// <returns></returns>
        /*public static async Task<bool> LicenseAction(string mGuid, string jMessage, int action = 0)
        {
            try
            {
                var httpClient = new HttpClient();
                var request = String.Format("{0}?a={1}&m={2}", Config.GetValue("LicenseValidityEndpoint"), action,
                    mGuid);

                Trace.TraceInformation("Отправка запроса: {0}", request);
                Trace.TraceInformation("Содержимое запроса: {0}", jMessage);

                var stringContent = new StringContent(jMessage, Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(request, stringContent);
                var bodyContent = await response.Content.ReadAsStringAsync();
                Trace.TraceInformation("Статус отправки {0},  содержимое: {1}", response.StatusCode,
                    (String.IsNullOrEmpty(bodyContent) ? "-" : bodyContent));

                var authObject = JsonConvert.DeserializeObject<LicInfo[]>(bodyContent);
                return (authObject.FirstOrDefault() == null || authObject.FirstOrDefault().IS_VALID == 1);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Исключение во время запуска LicenseAction: {0}", ex);
                return false;
            }
        }*/
    }
}