﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AvitoModels.Enums;
using AvitoModels.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SuperSocket.ClientEngine;
using WebSocket4Net;

namespace AvitoSocketApp
{
    public class SingleSocket : WebSocket
    {
        private readonly int AutoPingDelay = 30 * 1000; //Иначе авито автоматически закроет соединение через 60 секунд;
        private const string SocketOrigin = "https://www.avito.ru";
        private const string SocketServer = "wss://socket.avito.ru/socket"; //wss://socket.avito.ru/socket
        private const string UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36";
        public static int InstanceCounter = 0;
        private readonly int ReconnectionTimeout = 60 * 1000;
        public event EventHandler<EventArgs> ConnectionErrorOccurred;
        private CancellationTokenSource _autoPingCancellationSource = new CancellationTokenSource();
        public string UserId { get; set; }


        public SingleSocket(string userId, string cookieString)
            : base(SocketServer, null, StaticHelpers.GetCookieList(cookieString), CustomHeaders, UserAgent,
                SocketOrigin, WebSocketVersion.Rfc6455)
        {
            ++InstanceCounter;
            _instanceId = InstanceCounter;
            UserId = userId;

            Action<CancellationToken> autoPing = SendSocketAutoPing;

            base.Opened += (sender, args) => websocket_Opened(sender, args, autoPing);
            base.Error += new EventHandler<ErrorEventArgs>(websocket_Error);
            base.Closed += new EventHandler(websocket_Closed);
            base.MessageReceived += new EventHandler<MessageReceivedEventArgs>(websocket_MessageReceived);
        }

        private void ase(CancellationToken obj)
        {
            throw new NotImplementedException();
        }


        public T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public int SocketPacketIn { get; set; }

        public int SocketPacketOut { get; set; }

        public DateTime SocketOpened { get; set; }

        public Action<string> NewMessageNotifierAction { get; set; }

        private readonly int _instanceId;
        public string GetInstanceId()
        {
            return String.Format("{0:D10}", UserId);
        }

        public new void Send(string message)
        {
            if (base.State == WebSocketState.Open)
            {
                SocketPacketOut += 2;
                message = message.Replace("{0}", SocketPacketOut.ToString());
                Console.WriteLine("{0} O {1}", GetInstanceId(), message);
                if (IsValidJson(message))
                    base.Send(message);
            }
            else
            {
                Console.WriteLine("{0} M SingleSocket.Send - WebSocketState is not opened (base.State: {1})", GetInstanceId(), base.State.ToString());
            }
        }

        private static List<KeyValuePair<string, string>> CustomHeaders
        {
            get
            {
                return new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Sec-WebSocket-Key", Convert.ToBase64String(Encoding.UTF8.GetBytes(new Guid().ToString()))), //F04XHTP6yBzbWQjUKej9tQ==
                    new KeyValuePair<string, string>("Sec-WebSocket-Extensions", "permessage-deflate; client_max_window_bits"),
                    new KeyValuePair<string, string>("Upgrade", "_websocket")
                };
            }
        }

        public SingleSocket Open()
        {
            try
            {
                if (base.State != WebSocketState.Open && base.State != WebSocketState.Connecting)
                {
                    //BUG: вылетает исключение "The socket is connected, you needn't connect again!",
                    //при AllowConnectionRestore = true, хотя WebSocketState = Connecting
                    base.Open();
                }
                else
                {
                    Console.WriteLine("{0} M SingleSocket.Open - WebSocketState is already opened (base.State: {1})", GetInstanceId(), base.State.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E SingleSocket.Open - Exception: {1}", GetInstanceId(), ex);
            }
            return this;
        }

        public new void Close()
        {
            Console.WriteLine("{0} M SingleSocket.Close - WebSocket manual closing", GetInstanceId());
            base.Close();
            //this.Dispose();
        }

        public new void Close(int statusCode, string reason)
        {
            var msg = $"{reason} (Code: {statusCode})";
            var args = new ConnectionErrorEventArgs()
            { LastErrorTime = DateTime.UtcNow, ErrorMessage = msg, Status = statusCode };
            ConnectionErrorOccurred?.Invoke(this, args);
            Console.WriteLine("{0} M SingleSocket.Close - WebSocket manual closing", GetInstanceId());
            base.Close(statusCode, reason);
            //this.Dispose();
        }

        private void websocket_Opened(object sender, EventArgs eventArgs, Action<CancellationToken> autoPing = null)
        {
            SocketPacketOut = -1;
            SocketOpened = DateTime.UtcNow;
            _autoPingCancellationSource = new CancellationTokenSource();
            Console.WriteLine("{0} M SingleSocket.Opened", GetInstanceId());
            autoPing?.Invoke(_autoPingCancellationSource.Token);
        }

        private void websocket_Closed(object sender, EventArgs eventArgs)
        {
            try
            {
                _autoPingCancellationSource.Cancel();

                var msg = "Connection closed (empty EventArgs)";
                var code = (int)ConnectionStatusCodes.UndeterminedError;

                if (eventArgs is ClosedEventArgs cea)
                {
                    msg = $"{cea.Reason} (Code: {cea.Code})";
                    code = cea.Code;
                }

                Console.WriteLine("{0} M SingleSocket.Closed (Status code #{1}, Seconds online #{2:F0}, LastPacketIn #{3})",
                    GetInstanceId(), code, DateTime.UtcNow.Subtract(SocketOpened).TotalSeconds, SocketPacketIn);

                if (code != (int)ConnectionStatusCodes.AuthorizationError)
                {
                    Task.Delay(TimeSpan.FromMilliseconds(ReconnectionTimeout));
                    this.Open();
                    return;
                }

                var args = new ConnectionErrorEventArgs() { LastErrorTime = DateTime.UtcNow, ErrorMessage = msg, Status = code };
                ConnectionErrorOccurred?.Invoke(sender, args);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E SingleSocket.Closed - Exception: {1}", GetInstanceId(), ex);
            }
        }

        private void websocket_Error(object sender, ErrorEventArgs errorEventArgs)
        {
            Console.WriteLine("{0} E SingleSocket.Error: {1}", GetInstanceId(), errorEventArgs.Exception);
        }

        public async void SendSocketAutoPing(CancellationToken cancellationToken)
        {
            try
            {
                while (base.State == WebSocketState.Open)
                {
                    await Task.Run(async () => //Task.Run automatically unwraps nested Task types!
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(AutoPingDelay), cancellationToken);
                        PingMessage();
                    }, cancellationToken);
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
                Console.WriteLine("{0} M SendSocketAutoPing stopped", GetInstanceId());
                //_autoPingCancellationSource.Dispose();
            }
        }

        private async void websocket_MessageReceived(object sender, EventArgs eventArgs)
        {
            var socketResponse = ((MessageReceivedEventArgs)(eventArgs)).Message;
            try
            {
                Console.WriteLine("{0} I {1}", GetInstanceId(), socketResponse);
                JObject message = JObject.Parse(socketResponse);

                // Идентификатор сообщения (не ясно как отличить входящий от исходящего пакета)
                if (message.Property("id") != null)
                {
                    SocketPacketIn = message.Property("id").First.Value<int>();
                }

                var jsonType = (String)message.Property("type");
                if (jsonType == "Message")
                {
                    var messageObj = Deserialize<AvitoMessageIn>(socketResponse);
                    var senderId = messageObj.value.fromUid;
                    // Если это не сообщение отправлено не нами ( no echo ) и это не системное сообщение (обновите приложение)
                    if (UserId != senderId.ToString() && senderId != 0)
                    {
                        NewMessageNotifierAction(socketResponse);
                    }
                }
                //ConfirmMessage(SocketPacketIn);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E SingleSocket.MessageReceived - Exception: {1}", GetInstanceId(), ex);
            }
        }

        public bool IsValidJson(string json)
        {
            try
            {
                JToken token = JObject.Parse(json);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E SingleSocket.IsValidJson - JSON: {1}", GetInstanceId(), json);
                Console.WriteLine("{0} E SingleSocket.IsValidJson - Exception: {1}", GetInstanceId(), ex);
                return false;
            }
        }


        /// <summary>
        /// Отправка регулярных Ping сообщений с интервалом AutoPingDelay
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="messageText"></param>
        public void PingMessage()
        {
            var pingMessage = @"{""id"":{0},""method"":""ping"",""params"":{},""jsonrpc"":""2.0""}";
            this.Send(pingMessage);
        }


        /// <summary>
        /// Отправка сообщения адресату (пользователью авито)
        /// </summary>
        /// <param name="channelId">Идентификатор чата</param>
        /// <param name="messageText">Текст сообщения</param>
        public void SendMessage(string channelId, string messageText)
        {
            var newMessage = @"{""id"":{0},""method"":""avito.sendTextMessage"",""params"":{""channelId"":"""
                             + channelId
                             + @""",""text"":"""
                             + messageText.Replace("\r\n", "\\n").Replace("\"", "")
                             + @"""},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Получение значения прочитанных, непрочитанны сообщений в чате
        /// </summary>
        public void GetChatCounters()
        {
            var newMessage = @"{""id"":{0},""method"":""avito.getChatCounters"",""params"":{},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Получение открытых чатов с пользователями (лимит: 100шт.)
        /// </summary>
        public void GetChats()
        {
            var newMessage =
                @"{""id"":{0},""method"":""avito.getChats"",""params"":{""offset"":0,""limit"":100,""filters"":{}},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Получение информации по объявлению и его владельце по Id объявления
        /// </summary>
        /// <param name="adid"></param>
        public void GetAdInformation(int adid)
        {
            var newMessage = @"{""id"":{0},""method"":""avito.getBodyItems"",""params"":{""ids"":[" + adid +
                             @"]},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Получение последнего сообщения и информации о получателе/отправителе
        /// </summary>
        /// <param name="chid">u2i-1344419071-39185144</param>
        public void GetChatInformation(string chid)
        {
            var newMessage = @"{""id"":{0},""method"":""avito.getChatById"",""params"":{""channelId"":""" + chid +
                             @"""},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Получение истории сообщений из чата
        /// </summary>
        /// <param name="chid">u2i-1344419071-39185144</param>
        public void GetMessageHistory(int chid)
        {
            var newMessage = @"{""id"":{0},""method"":""messenger.history"",""params"":{""channelId"":""" + chid +
                             @"""},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Сделать сообщения из чата "прочитанными"
        /// </summary>
        /// <param name="chid">u2i-1344419071-39185144</param>
        public void SetChatAsDelivered(int chid)
        {
            var newMessage = @"{""id"":{0},""method"":""messenger.markChatsAsDelivered"",""params"":{""channelId"":""" +
                             chid +
                             @"""},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }

        /// <summary>
        /// Отправка подтверждения на сервер о получении сообщения
        /// </summary>
        /// <param name="id"></param>
        public void ConfirmMessage(int id)
        {
            var newMessage = @"{""id"":{0},""method"":""acks"",""params"":{""ids"":[" + id +
                             @"]},""jsonrpc"":""2.0""}";
            this.Send(newMessage);
        }
    }

    public class ConnectionErrorEventArgs : EventArgs
    {
        public DateTime LastErrorTime { get; set; }
        public string ErrorMessage { get; set; }
        public int Status { get; set; }
    }

    public class SingleSocketComparer : IEqualityComparer<SingleSocket>
    {
        public bool Equals(SingleSocket x, SingleSocket y)
        {
            return (x.UserId == y.UserId);
        }
        public int GetHashCode(SingleSocket obj)
        {
            return obj.UserId.GetHashCode();
        }
    }
}

/*
// Получение
// ↓ (открытие в браузере)
{"id":2,"type":"ChatRead","value":{"uid":39185144,"channelId":"u2i-929066238-2831187","fromUid":2831187,"time":15185938445375046}}

// ↑ наше подтверждение
{"id":11,"method":"acks","params":{"ids":[2]},"jsonrpc":"2.0"}

// ↓ получение от сервера нового сообщения
{"id":3,"type":"Message","value":{"uid":39185144,"id":"05de56866b57d1e5ea549585f3a5f4fb","body":{"text":"привет иколайн, продайте телефон!"},"channelId":"u2i-929066238-2831187","type":"text","created":15185938604092761,"fromUid":2831187,"isDeleted":false,"isRead":false,"isSpam":false,"expireAt":"0001-01-01T00:00:00Z"}}

// ↑ наше подтверждение
{"id":15,"method":"acks","params":{"ids":[3]},"jsonrpc":"2.0"}

  
// Отправка
// ↓ чат прочтен (открытие в браузере)
{"id":2,"type":"ChatRead","value":{"uid":2831187,"channelId":"u2i-929066238-2831187","fromUid":2831187,"time":15185938445375046}}

// ↑ отправка сообщения через чат
{"id":17,"method":"avito.sendTextMessage","params":{"channelId":"u2i-929066238-2831187","text":"привет иколайн, продайте телефон!"},"jsonrpc":"2.0"}

// ↓ сообщение пересылается обратно отправителю
{"id":3,"type":"Message","value":{"uid":2831187,"id":"05de56866b57d1e5ea549585f3a5f4fb","body":{"text":"привет иколайн, продайте телефон!"},"channelId":"u2i-929066238-2831187","type":"text","created":15185938604092761,"fromUid":2831187,"isDeleted":false,"isRead":true,"isSpam":false,"expireAt":"0001-01-01T00:00:00Z"}}

// ↓ Сообщение помечается прочитанным
{"id":4,"type":"ChatDelivered","value":{"uid":2831187,"channelId":"u2i-929066238-2831187","fromUid":39185144,"time":15185938607076930}}
 */

/*
запрос на получение телефона
{"id":43,"method":"avito.getPhoneByItemId.v1","params":{"itemId":1583833808,"chatId":"u2i-1583833808-2831187"},"jsonrpc":"2.0"}
{"id":43,"jsonrpc":"2.0","result":{"phone":"+7 911 967-52-92"}}

делаем сообщения прочитанными
{"id":49,"method":"messenger.markMessagesAsDelivered","params":{"channelId":"u2i-1583833808-2831187","ids":["20130eafbe4f87099eb2ce8ecf906675"]},"jsonrpc":"2.0"}
{"id":49,"jsonrpc":"2.0","result":true}


{"id":91,"method":"messenger.getLastActionTimes","params":{"userIds":[39185144]},"jsonrpc":"2.0"}
//длинный ответ
 *
 */
