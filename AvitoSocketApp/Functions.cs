﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AvitoModels.Enums;
using AvitoModels.Hub;
using AvitoModels.Sellavi;
using AvitoModels.Sockets;
using AvitoModels.TableEntities;
using CloudTableExtensions;
using Microsoft.WindowsAzure.Storage.Table;

namespace AvitoSocketApp
{
    [ErrorHandler]
    public class Functions
    {

        private readonly IAvitoSocketFactory _socketFactory;
        private readonly IConfiguration _config;
        private readonly ILogger _logger;
        private readonly CloudTableClient _tableClient;

        public Functions(IAvitoSocketFactory socketFactory, IConfiguration config, CloudTableClient tableClient, ILogger<Functions> logger)
        {
            _socketFactory = socketFactory;
            _config = config;
            _logger = logger;
            _tableClient = tableClient;
        }

        /// <summary>
        /// Отправка сообщения из очереди (от Бота в Чат Авито)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logger"></param>
        [MessageValidator]
        public async Task ProcessNewMessage([QueueTrigger("avito-message-queue")] AvitoMessageOut message)
        {
            try
            {
                _logger.LogInformation($"Processing avito-message-queue item: {message}");
                var chid = message.@params.channelId;
                var msg = message.@params.text;
                var userId = message.userId;
                var connection = await _socketFactory.RetrieveFromPool(userId);
                connection.SendMessage(chid, msg);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Functions.ProcessNewMessage exception: ");
            }
        }

        /// <summary>
        /// Не используется (установка нового socket подключения при появлении нового блоба в хранилище)
        /// </summary>
        /// <param name="blob"></param>
        /// <param name="blobName"></param>
        /// <param name="metaData"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public async Task ProcessBlobAsync( /*[BlobTrigger("authorized-blobs/{blobName}")] - off*/ CloudBlockBlob blob, string blobName, IDictionary<string, string> metaData, ILogger logger)
        {
            try
            {
                //TODO: сделать анализ metaData и обработать только блобы где creator = avitostarbot
                logger.LogInformation("BlobTrigger fired with blob: {0}", blobName);

                var mGuid = blobName.Substring(0, blobName.IndexOf('.'));
                var userId = new System.Guid(mGuid.Replace("-", ""));

                /*
                var httpClient = new HttpClient();
                var botEndPoint = _config.GetSection("AppSettings:BotUserListUrl").Value;
                var response = await httpClient.GetAsync(botEndPoint);
                //response.statuscode:503, bodyContent:"The service is unavailable" -> JsonReaderException
                var bodyContent = await response.Content.ReadAsStringAsync();
                var botUsers = JsonConvert.DeserializeObject<List<Microsoft.Bot.Schema.ChannelAccount>>(bodyContent);

                var predicate = new Predicate<Microsoft.Bot.Schema.ChannelAccount>((x) => { return userId.ToString() == x.Id; });
                if (botUsers.Exists(predicate))
                {*/
                var blobContent = await blob.DownloadTextAsync();
                var users = JsonConvert.DeserializeObject<List<SellaviUser>>(blobContent);
                foreach (var user in users)
                {
                    var connectionEntity = new AvitoConnection(userId.ToString(), user.ProfileId.ToString()) {ConnectionCookie = user.UserCookie};
                    await _socketFactory.CreateNewConnectionAsync(connectionEntity);
                }

                // }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Functions.ProcessBlobAsync exception: ");
            }
        }
        /// <summary>
        /// https://docs.microsoft.com/en-us/azure/azure-functions/functions-bindings-storage-table?tabs=csharp
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pocos"></param>
        /// <param name="log"></param>
        public static void ProcessConnectionTable([Table("AvitoConnection", "*", "*")] AvitoConnection connection, ILogger logger)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Functions.ProcessConnectionTable exception: ");
            }
        }

        /// <summary>
        /// Каждые 60 секунд следим за изменениями в таблице подключений
        /// {second} {minute} {hour} {day} {month} {day-of-week}
        /// <see cref="https://arminreiter.com/2017/02/azure-functions-time-trigger-cron-cheat-sheet/"/>
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="connection"></param>
        /// <param name="logger"></param>
        public async Task ProcessConnections([TimerTrigger("*/60 * * * * *")] TimerInfo myTimer, ILogger logger)
        {
            try
            {
                var searchPredicate = TableQuery.CombineFilters(
                    TableQuery.GenerateFilterConditionForInt("ConnectionStatus", QueryComparisons.Equal, ((int) ConnectionStatusCodes.ActiveConnection)),
                    TableOperators.And,
                    TableQuery.GenerateFilterConditionForDate("ConnectionStarted", QueryComparisons.Equal, AvitoConnection.EmptyDateTime)
                );


                var entities = await CloudTableUtils.ExecuteQuerySegmentedAsync<AvitoConnection>(_tableClient, new TableQuery().Where(searchPredicate));

                foreach (var entitiy in entities)
                {
                    try
                    {
                        if (entitiy is AvitoConnection)
                        {
                            var notificationClientUrl = _config.GetSection("AppSettings:BotNotificationMessage").Value + "/" + entitiy.PartitionKey;
                            var message = $"Поздравляем, мы только-что подключили вашу учетную запись **{entitiy.RowKey}**. Теперь бот вас будет уведомлять о всех входящих сообщениях!";

                            var activated = await _socketFactory.CreateNewConnectionAsync(entitiy);
                            if (!activated)
                            {
                                message = $"Ошибка подключения учетной записи **{entitiy.RowKey}**";
                                entitiy.ConnectionStatus = (int) ConnectionStatusCodes.UnableToStart;
                                entitiy.ConnectionErrorMessage = $"Connection error: {entitiy.RowKey}";
                                entitiy.ConnectionStarted = AvitoConnection.EmptyDateTime;
                                entitiy.ConnectionErrorDate = DateTime.UtcNow;
                                await CloudTableUtils.InsertOrMergeEntityAsync(_tableClient, entitiy);
                            }

                            var jMessage = JsonConvert.SerializeObject(new HubMessageText(message));
                            await HttpNotifier.NotifyClient(notificationClientUrl, jMessage);
                        }
                    }
                    catch (Exception iException)
                    {
                        logger.LogError(iException, "Functions.ProcessConnections inner exception: ");
                    }
                }

                // Возможно очистка не требуется, т.к.
                _socketFactory.CleanUpConnectionPool();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Functions.ProcessConnections exception: ");
            }
        }

        /// <summary>
        /// В 10:00 по МСК(+3) напоминаем клиенту о необходимости переподключить аккаунт
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="logger"></param>
        public async Task ProcessInvalidConnections([TimerTrigger("0 0 7 * * *")] TimerInfo myTimer, ILogger logger)
        {
            try
            {
                var searchPredicate = TableQuery.CombineFilters(
                    TableQuery.GenerateFilterConditionForInt("ConnectionStatus", QueryComparisons.NotEqual, ((int) ConnectionStatusCodes.ActiveConnection)),
                    TableOperators.And,
                    TableQuery.GenerateFilterConditionForDate("ConnectionErrorDate", QueryComparisons.GreaterThan, DateTime.UtcNow.AddHours(-120))
                );

                var entities = await CloudTableUtils.ExecuteQuerySegmentedAsync<AvitoConnection>(_tableClient, new TableQuery().Where(searchPredicate));
                foreach (var entitiy in entities)
                {
                    try
                    {
                        if (entitiy is AvitoConnection)
                        {
                            var notificationClientUrl = _config.GetSection("AppSettings:BotNotificationDialog").Value + "/" + entitiy.PartitionKey;
                            var message = $"ВНИМАНИЕ, ваша учетная запись **{entitiy.RowKey}** отключена! Вы не сможете получать уведомления о новых сообщениях. Зайдите в раздел **Мои аккаунты** для переподключения.";

                            var jMessage = JsonConvert.SerializeObject(new HubMessageDialog(message));
                            await HttpNotifier.NotifyClient(notificationClientUrl, jMessage);
                        }
                    }
                    catch (Exception iException)
                    {
                        logger.LogError(iException, "Functions.ProcessInvalidConnections inner exception: ");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Functions.ProcessInvalidConnections exception: ");
            }

        }
    }
}
