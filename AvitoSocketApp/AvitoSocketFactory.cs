﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AvitoModels.Enums;
using AvitoModels.TableEntities;
using CloudTableExtensions;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Table;
using WebSocket4Net;

namespace AvitoSocketApp
{
    public class AvitoSocketFactory : IAvitoSocketFactory
    {
        private readonly IConfiguration _config;
        private readonly CloudTableClient _tableClient;

        public AvitoSocketFactory(IConfiguration config, CloudTableClient tableClient)
        {
            _config = config;
            _tableClient = tableClient;
        }

        // Maximum objects allowed!
        private static int _PoolMaxSize = 500;

        // My Collection Pool
        private static readonly HashSet<SingleSocket> objPool = new HashSet<SingleSocket>(_PoolMaxSize, new SingleSocketComparer());

        // Collection Lock
        private readonly object hashSetLock = new object();

        public async Task<bool> CreateNewConnectionAsync(AvitoConnection connectionEntity)
        {
            try
            {
                // Создание подключения SingleSocket и добавление в objPool
                StartNewSocketSession(connectionEntity);

                // Сохранение информации о запущенном подключении
                connectionEntity.ConnectionStatus = (int)ConnectionStatusCodes.ActiveConnection;
                connectionEntity.ConnectionErrorMessage = String.Empty;
                connectionEntity.ConnectionStarted = DateTime.UtcNow;
                connectionEntity.ConnectionErrorDate = AvitoConnection.EmptyDateTime;
                await CloudTableUtils.InsertOrMergeEntityAsync(_tableClient, connectionEntity);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E CreateNewConnectionAsync.Exception: {1}", "FACTORY_INFO", ex);
                return false;
            }
        }

        public bool StartNewSocketSession(AvitoConnection connectionEntity)
        {
            // Создаем новое подключение
            var ss = new SingleSocket(connectionEntity.RowKey, connectionEntity.ConnectionCookie);
            var notificationUrl = _config.GetSection("AppSettings:BotNotificationAvito").Value + "/" + connectionEntity.PartitionKey;
            ss.ConnectionErrorOccurred += (sender, args) => OnConnectionErrorOccurred(sender, args, connectionEntity);
            ss.NewMessageNotifierAction = async (message) => { await HttpNotifier.NotifyClient(notificationUrl, message); };

            // Открываем подключение и добавляем в objPool
            lock (hashSetLock)
            {
                if (objPool.Add(ss))
                {
                    ss.Open();
                }
                else
                {
                    ss.Dispose();
                }
            }

            Process currentProc = Process.GetCurrentProcess();
            Console.WriteLine("{0} M Object pool usage: {1} / {2}", connectionEntity.RowKey, objPool.Count, _PoolMaxSize);
            Console.WriteLine("{0} M Memory usage: {1}K", connectionEntity.RowKey, currentProc.PrivateMemorySize64 / 1024);
            return true;
        }

        /// <summary>
        /// Выкидываем подключение из пула, помечаем как завершенное с ошибкой (-1)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnConnectionErrorOccurred(object sender, EventArgs e, AvitoConnection connectionEntity)
        {
            var er = e as ConnectionErrorEventArgs;
            var ss = (SingleSocket)sender;

            RemoveFromPool(ss);

            Process currentProc = Process.GetCurrentProcess();
            Console.WriteLine("{0} M Object pool usage: {1} / {2}", connectionEntity.RowKey, objPool.Count, _PoolMaxSize);
            Console.WriteLine("{0} M Memory usage: {1}K", connectionEntity.RowKey, currentProc.PrivateMemorySize64 / 1024);

            connectionEntity.ConnectionStatus = er.Status;
            connectionEntity.ConnectionErrorMessage = er.ErrorMessage;
            connectionEntity.ConnectionErrorDate = er.LastErrorTime;

            await CloudTableUtils.InsertOrMergeEntityAsync(_tableClient, connectionEntity);
        }


        /// <summary>
        /// Метод вызывается при помощи QueueTrigger для последующей отправки сообщения
        /// </summary>
        /// <param name="chid">Идентификатор открытого чата авито <see cref="AvitoUserChid"/></param>
        /// <returns>Открытое websocket соединение см. объект <see cref="SingleSocket"/></returns>
        public async Task<SingleSocket> RetrieveFromPool(string userId)
        {
            try
            {
                return objPool.FirstOrDefault(x => x.UserId == userId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E RetrieveFromPool.Exception: {1}", "FACTORY_INFO", ex);
                throw;
            }
        }


        public async void PopulateConnectionPool()
        {
            var searchPredicate = TableQuery.GenerateFilterConditionForInt("ConnectionStatus", QueryComparisons.Equal,
                ((int)ConnectionStatusCodes.ActiveConnection));

            var entities = await CloudTableUtils.ExecuteQuerySegmentedAsync<AvitoConnection>(_tableClient, new TableQuery().Where(searchPredicate));

            foreach (var entitiy in entities)
            {
                try
                {
                    if (entitiy is AvitoConnection)
                    {
                        await CreateNewConnectionAsync(entitiy);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("{0} E PopulateConnectionPool.Exception: {1}", "FACTORY_INFO", ex);
                    throw;
                }
            }
        }

        public void RemoveFromPool(AvitoConnection connectionEntity)
        {
            try
            {
                // Вероятно требуется предварительно вызвать socket.Close / socket.Dispose();
                foreach (var singleSocket in objPool.Where(x => x.UserId == connectionEntity.RowKey))
                {
                    RemoveFromPool(singleSocket);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E RemoveFromPool.Exception: {1}", "FACTORY_INFO", ex);
                throw;
            }
        }

        public void RemoveFromPool(SingleSocket singleSocket)
        {
            lock (hashSetLock)
                objPool.Remove(singleSocket);

            SingleSocket.InstanceCounter--;
            Console.WriteLine("{0} M SingleSocket connection removed from the pool", singleSocket.GetInstanceId());
        }

        public async void CleanUpConnectionPool()
        {
            try
            {
                // Не проверено выполняются-ли эти условия когда-либо
                var invalidConnections = objPool.Where(x => x.SocketOpened == null || x.SocketPacketIn == 0 || x.State != WebSocketState.Open);
                var singleSockets = invalidConnections as SingleSocket[] ?? invalidConnections.ToArray();
                foreach (var invc in singleSockets)
                {
                    //RemoveFromPool(invc);
                    Console.WriteLine("{0} M Orphaned connection found: {1} (Opened at #{2}, LastPacketIn #{3}, State #{4})",
                        "FACTORY_INFO", invc.GetInstanceId(), invc.SocketOpened, invc.SocketPacketIn, invc.State);
                }

                // Отключение пользователя по собственному желанию (DeactivatedByUser) за последние сутки
                var searchPredicate = TableQuery.CombineFilters(
                    TableQuery.GenerateFilterConditionForInt("ConnectionStatus", QueryComparisons.Equal, ((int)ConnectionStatusCodes.DeactivatedByUser)),
                    TableOperators.And,
                    TableQuery.GenerateFilterConditionForDate("Timestamp", QueryComparisons.GreaterThan, DateTime.UtcNow.AddHours(-24))
                );
                var entities = await CloudTableUtils.ExecuteQuerySegmentedAsync<AvitoConnection>(_tableClient, new TableQuery().Where(searchPredicate));
                foreach (var entity in entities)
                {
                    RemoveFromPool(entity);
                }

                // Уведомление
                if (singleSockets.Any())
                {
                    Process currentProc = Process.GetCurrentProcess();
                    Console.WriteLine("{0} M Object pool cleanup: {1} / {2}", "FACTORY_INFO", objPool.Count, _PoolMaxSize);
                    Console.WriteLine("{0} M Memory usage: {1}K", "FACTORY_INFO", currentProc.PrivateMemorySize64 / 1024);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} E CleanUpConnectionPool.Exception: {1}", "FACTORY_INFO", ex);
            }
        }

        public void ShutDownAllConnections()
        {
            foreach (var ss in objPool.ToList())
            {
                ss.Close((int)ConnectionStatusCodes.ApplicationIsClosing, "App is stopping");
                RemoveFromPool(ss);
            }
        }
    }
}