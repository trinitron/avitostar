using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AvitoModels.Sellavi
{
    [DataContract]
    public class SellaviUser
    {
        [DataMember]
        [JsonProperty("id")]
        public int ProfileId { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string UserPassword { get; set; }

        [DataMember]
        public string UserCookie { get; set; }

        [DataMember]
        [JsonProperty("name")]
        public string CompanyName { get; set; }

        [DataMember]
        public string CompanyManager { get; set; }

        [DataMember]
        [JsonProperty("email")]
        public string CompanyEmail { get; set; }

        [DataMember]
        [JsonProperty("phone")] //TODO ��������� ����������� � ������� ��� �������� ����� ����� � ������� (8 4852 23-03-28;8 4852 33-60-00)
        public string CompanyPhones { get; set; }

        [IgnoreDataMember]
        public string FormattedUserName
        {
            get
            {
                if (UserName == null)
                    return null;

                var n = UserName.ToLower().Trim();
                if (n.Contains("@"))
                {
                    return n;
                }
                // ���������������� � sellavi.model
                n = n.Replace(" ", "");
                n = n.Replace("(", "");
                n = n.Replace(")", "");
                n = n.Replace("-", "");
                n = n.Replace("+7", "8");

                if (n.Length == 10 && !n.StartsWith("8"))
                {
                    n = "8" + n;
                }

                if (n.Length == 11 && n.StartsWith("7"))
                {
                    n = "8" + n.Substring(1);
                }

                return n;
            }
        }
    }

    /*     Avito Json User Schema
     *     public class User
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int locationId { get; set; }
        public int metroId { get; set; }
        public int id { get; set; }
        public bool isPro { get; set; }
        public bool isLegalPerson { get; set; }
        public string userHashId { get; set; }
        public Object type { get; set; }
    }
     */
}
