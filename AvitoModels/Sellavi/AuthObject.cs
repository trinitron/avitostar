﻿using System;

namespace AvitoModels.Sellavi
{
    //{"status":"blocked-account","result":{"userDialog":{"title":"Профиль заблокирован","message":"Причина — нарушение правил"}}}
    //{"status":"error-dialog","result":{"userDialog":{"title":"Восстановление доступа","message":"Для вашей безопасности мы сбросили пароль от учётной записи с телефоном +7 920 232-09-05.","actions":[{"title":"Задать новый пароль","uri":"ru.avito://1/resetPassword?login=%2B7+920+232-09-05"}],"cancelable":false}}}
    //{"status":"antihack-check-phones","result":{"challengeId":"4fd618b1bc1744a681e2736566bcbc22"}}
    //{"status":"antihack-check","result":{"phoneMask":"+7 952 095-**-**","challengeId":"7bb91ab92fb64b03bfd8f50df6f76c4e"}}
    //{"status":"antihack-check","result":{"messages":null,"user":null,"userDialog":null,"session":null,"signature":null,"phash":null,"refreshToken":null,"phoneMask":"+7 905 266-**-**","challengeId":"d7b71fd23a6640569e4a24e0327b1efd"}}
    //{"status":"incorrect-data","result":{"messages":{"login":"Введите корректный телефон или электронную почту"}}}
    //{"status":"wrong-credentials","result":{"userDialog":{"title":"Не удаётся войти","message":"Неправильная почта"}}}
    //{"status":"wrong-credentials","result":{"userDialog":{"title":"Не удаётся войти","message":"Неправильный пароль"}}}
    //{"status":"incorrect-data","result":{"messages":{"password":"Заполните поле"}}}
    //{"status":"incorrect-data","result":{"messages":{"password":"Заполните поле","login":"Введите корректный email"}}}
    //{"status":"incorrect-data","result":{"messages":{"password":"Заполните поле","login":"Введите корректный телефон или электронную почту"}}}
    //{"status":"ok","result":{"user":{"name":"Иван","phone":"8 905 266-14-87","email":"vovano2006@yandex.ru","locationId":653240,"metroId":171,"id":2831187,"isPro":false,"isLegalPerson":false,"userHashId":"fb6b1236dd0da676b125347add68fe37","type":{"code":"company","title":"Компания"}},"session":"c1a5926c0823a4811743587b2bfd082c.1560014537","signature":"afafafafafafafafafafafafafafafaf","phash":"70bf3a6ae8776221313b16ed5b13095d","refreshToken":"3f0fd55d93ac26d91c5e50f90b974820"}}
    //{"status":"ok","result":{"user":{"name":"?????? ?????? ??? ? ??????","phone":"8 910 380-53-66","email":"ecpafile1974@rambler.ru","locationId":653241,"districtId":26,"id":121197314,"isPro":false,"isLegalPerson":false,"userHashId":"b02ca2301fb7f0f2d1a418830f430317","type":{"code":"private","title":"??????? ????"},"shop":{"shopId":223540,"name":""}},"session":"88df75c386911db74b18128790a853a5.1564519363","signature":"afafafafafafafafafafafafafafafaf","phash":"d3337dcfabfb0e4c21a413d75d3aff96","refreshToken":"e4062a32326a2dad46ac3e26b9abf8bb"}}
    //{"error":{"message":"?????? ? ?????? IP-?????? ???????? ?????????","link":"ru.avito://1/info/ipblock/show","code":403}}
    public class Messages
    {
        public string password { get; set; }
        public string login { get; set; }
        /// <summary>
        /// Искуственное поле для передачи соощений клиенту
        /// </summary>
        public string info { get; set; }
    }

    /// <summary>
    /// Если AuthObject.status='ok' - значит авторизация прошла успешно.
    /// user, session, signature, phash, refreshToken при этом должны быть заполнены.
    /// </summary>
    public class Result
    {
        public Messages messages { get; set; }
        public SellaviUser user { get; set; }
        public UserDialog userDialog { get; set; }
        public string session { get; set; }
        public string signature { get; set; }
        public string phash { get; set; }
        public string refreshToken { get; set; }
        public string phoneMask { get; set; }
        public string challengeId { get; set; }
    }

    public class UserDialog
    {
        public string title { get; set; }
        public string message { get; set; }
    }

    public class Error
    {
        public string message { get; set; }
        public string link { get; set; }
        public int code { get; set; }
    }

    //[DataContract]
    public class AuthObject
    {
        public string status { get; set; }
        public Result result { get; set; }
        public Error error { get; set; }

        public AuthObject(SellaviUser user)
        {
            this.result = new Result();
            result.user = user;
            status = "ok";
        }

        public AuthObject(string infoMessage)
        {
            this.result = new Result();
            result.messages = new Messages();
            result.messages.info = infoMessage;
        }

        public AuthObject()
        {
        }

        public string GetMessage()
        {
            var strError = result?.userDialog?.message ??
                           result?.messages?.login ??
                           result?.messages?.password ??
                           result?.messages?.info ??
                           status;
            return strError;
        }
    }




    public class User
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int locationId { get; set; }
        public int metroId { get; set; }
        public int id { get; set; }
        public bool isPro { get; set; }
        public bool isLegalPerson { get; set; }
        public string userHashId { get; set; }
        public Object type { get; set; }
    }





}
