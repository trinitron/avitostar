﻿using System.Text.RegularExpressions;

namespace AvitoModels.Sockets
{
    public class Value
    {
        public int uid { get; set; }
        public string id { get; set; }
        public Body body { get; set; }
        public string channelId { get; set; }
        public string type { get; set; }
        public long created { get; set; }
        public int fromUid { get; set; }
        public bool isDeleted { get; set; }
        public bool isRead { get; set; }
        public bool isSpam { get; set; }
        public bool isFirstMessage { get; set; }
        public string chatType { get; set; }
        public Channel channel { get; set; }


        public string AdId
        {
            get
            {
                if (channelId != null)
                {
                    var reg = Regex.Match(channelId, @"u2i-(?<item>\d+)-(?<user>\d+)");
                    var user = reg.Groups["user"].Value;
                    var item = reg.Groups["item"].Value;
                    return item;
                }

                return null;
            }
        }

    }


}
