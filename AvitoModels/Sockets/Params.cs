﻿namespace AvitoModels.Sockets
{
    public class Params
    {
        public string channelId { get; set; }
        public string text { get; set; }
    }
}