﻿using System;

namespace AvitoModels.Sockets
{
    public class ChannelAccount
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string AadObjectId { get; set; }

        public string Role { get; set; }

        public Object Properties { get; set; }
    }
}