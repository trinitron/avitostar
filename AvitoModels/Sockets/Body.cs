﻿namespace AvitoModels.Sockets
{
    public class Body
    {
        public string randomId { get; set; }
        public string text { get; set; }
    }
}