﻿namespace AvitoModels.Sockets
{
    public class AvitoMessageOut
    {
        public AvitoMessageOut()
        {
            this.@params = new Params();
        }
        public int id { get; set; } = 0;    //the value will be autopopulated by webjob
        public string method { get; set; } = "avito.sendTextMessage";
        public Params @params { get; set; }
        public string jsonrpc { get; set; } = "2.0";
        public string userId { get; set; }
    }
}
