﻿using Microsoft.WindowsAzure.Storage.Table;

namespace AvitoModels.TableEntities
{
    public class AvitoBotReference : TableEntity
    {
        public string ConversationReference { get; set; }
        public AvitoBotReference()
        { }

        public AvitoBotReference(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

    }
}
