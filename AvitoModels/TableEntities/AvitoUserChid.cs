﻿using Microsoft.WindowsAzure.Storage.Table;

namespace AvitoModels.TableEntities
{
    public class AvitoUserChid : TableEntity
    {
        public AvitoUserChid()
        { }

        public AvitoUserChid(string UserId, string Chid)
        {
            PartitionKey = UserId;
            RowKey = Chid;
        }

    }
}
