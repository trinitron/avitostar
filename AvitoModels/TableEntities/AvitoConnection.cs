﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace AvitoModels.TableEntities
{
    public class AvitoConnection : TableEntity
    {
        [IgnoreProperty]
        public static DateTime EmptyDateTime { get; set; } =
            new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

        public int ConnectionStatus { get; set; } = 0;
        public string ConnectionCookie { get; set; }
        public DateTime ConnectionStarted { get; set; } = EmptyDateTime;
        public string ConnectionErrorMessage { get; set; }
        public DateTime ConnectionErrorDate { get; set; } = EmptyDateTime;

        public AvitoConnection()
        { }

        public AvitoConnection(string UserId, string AvitoProfileId)
        {
            PartitionKey = UserId;
            RowKey = AvitoProfileId;
        }

    }
}
