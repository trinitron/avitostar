﻿namespace AvitoModels.Enums
{
    public enum ConnectionStatusCodes : int
    {
        ActiveConnection = 0,
        ReconnectionLimitReached = -1,
        ApplicationIsClosing = -2,
        UndeterminedError = -3,
        DeactivatedByUser = -4,
        UnableToStart = -5,
        AuthorizationError = 3401
    }
}
