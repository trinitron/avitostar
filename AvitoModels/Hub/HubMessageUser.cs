﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AvitoModels.Sellavi;

namespace AvitoModels.Hub
{
    [DataContract]
    public class HubMessageUser : HubMessage
    {
        [DataMember]
        public SellaviUser AvitoUser { get; set; }

        [DataMember]
        public bool ReAuthRequired { get; set; }

        [DataMember]
        public string VerificationCode { get; set; }

        [DataMember]
        public Uri ProxySettings { get; set; }

        [DataMember]
        public override string MessageType { get; set; }

        public HubMessageUser(SellaviUser user)
        {
            this.AvitoUser = user;
            this.MessageType = this.GetType().Name;
        }
    }

    /// <summary>
    /// Keep it synchronized with AuthorizationModule.Webjob.Models
    /// </summary>
    public class HubMessageUserComparer : IEqualityComparer<HubMessageUser>
    {
        public bool Equals(HubMessageUser x, HubMessageUser y)
        {
            return (x.MessageOwnerGuid == y.MessageOwnerGuid &&
                    x.AvitoUser.FormattedUserName == y.AvitoUser.FormattedUserName &&
                    x.ReAuthRequired == y.ReAuthRequired &&
                    x.VerificationCode == y.VerificationCode);
        }
        public int GetHashCode(HubMessageUser obj)
        {
            return obj.MessageOwnerGuid.GetHashCode() ^
                   obj.AvitoUser.FormattedUserName.GetHashCode() ^
                   obj.ReAuthRequired.GetHashCode() ^
                   (obj.VerificationCode ?? "").GetHashCode();
        }
    }
}
