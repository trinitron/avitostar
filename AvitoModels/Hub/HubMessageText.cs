﻿using System.Runtime.Serialization;

namespace AvitoModels.Hub
{
    [DataContract]
    public class HubMessageText : HubMessage
    {
        [DataMember]
        public string Text { get; set; }

        public HubMessageText(string text)
        {
            this.MessageType = this.GetType().Name;
            this.Text = text;
        }

        public override string MessageType { get; set; }
    }
}
