﻿using System.Runtime.Serialization;

namespace AvitoModels.Hub
{
    [DataContract]
    public class HubMessageDialog : HubMessage
    {
        [DataMember]
        public string Dialog { get; set; }
        [DataMember]
        public object Options { get; set; }

        public HubMessageDialog(string dialog)
        {
            this.MessageType = this.GetType().Name;
            this.Dialog = dialog;
        }

        public override string MessageType { get; set; }
    }
}
