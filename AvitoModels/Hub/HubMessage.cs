﻿using System;
using System.Runtime.Serialization;

namespace AvitoModels.Hub
{
    [DataContract]
    public abstract class HubMessage
    {
        private string _mGuid = Guid.NewGuid().ToString();
        [DataMember]
        public string MessageGuid
        {
            set { _mGuid = value; }
            get { return _mGuid; }
        }

        private string _mOwnerGuid;
        [DataMember]
        public string MessageOwnerGuid
        {
            set { _mOwnerGuid = value; }
            get { return _mOwnerGuid; }
        }

        private DateTime _mDate = DateTime.UtcNow;
        [DataMember]
        public DateTime MessageDate
        {
            set { _mDate = value; }
            get { return _mDate; }
        }

        private string _mCreator = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        [DataMember]
        public string MessageCreator
        {
            set { _mCreator = value; }
            get { return _mCreator; }
        }

        private Uri _mCallbackUrl;
        [DataMember]
        public Uri MessageCallbackUrl
        {
            set { _mCallbackUrl = value; }
            get { return _mCallbackUrl; }
        }

        [DataMember]
        public abstract string MessageType { get; set; }

    }
}
