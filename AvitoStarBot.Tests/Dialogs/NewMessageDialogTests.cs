﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Threading.Tasks;
using AvitoModels.Sockets;
using AvitoStarBot.Dialogs;
using AvitoStarBot.Tests.Common;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Testing;
using Microsoft.Bot.Builder.Testing.XUnit;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace AvitoStarBot.Tests.Dialogs
{
    public class NewMessageDialogTests : BotTestBase
    {
        private readonly IMiddleware[] _middlewares;

        public NewMessageDialogTests(ITestOutputHelper output)
            : base(output)
        {
            _middlewares = new IMiddleware[] { new XUnitDialogTestLogger(output) };
        }

        [Fact]
        public async Task DialogFlowUseCases()
        {
            // Arrange
            var message = ResourceHelper.GetEmbeddedResource("Dialogs\\TestData\\NewMessageDialog.json");
            var askReply = new string[,]
            {
                {null, "Новое сообщение"},
                {null, "Новое сообщение"},
                {null, "Новое сообщение"},
            };

            var sut = new NewMessageDialog();
            var testClient = new DialogTestClient(Channels.Telegram, sut, JsonConvert.DeserializeObject<AvitoMessageIn>(message), _middlewares);

            // Execute the test case
            for (var i = 0; i < askReply.GetLength(0); i++)
            {
                var reply = await testClient.SendActivityAsync<IMessageActivity>(askReply[i, 0]);
                Assert.NotEmpty(reply.Text);
            }
        }
    }
}
