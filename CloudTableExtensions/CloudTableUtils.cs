﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CloudTableExtensions
{
    /// <summary>
    /// https://github.com/Azure-Samples/storage-table-dotnet-getting-started
    /// </summary>
    /// <typeparam name="T">TableEntity</typeparam>
    public static class CloudTableUtils
    {
        /// <summary>
        /// Demonstrate the most efficient storage query - the point query - where both partition key and row key are specified.
        /// </summary>
        /// <param name="tableClient">Sample table name</param>
        /// <param name="partitionKey">Partition key - i.e., last name</param>
        /// <param name="rowKey">Row key - i.e., first name</param>
        /// <returns>A Task object</returns>
        /// public T GetDetachedEntity<T>(T entityObject) where T : class
        public static async Task<T> RetrieveEntityUsingPointQueryAsync<T>(CloudTableClient tableClient, string partitionKey, string rowKey) where T : TableEntity
        {
            try
            {
                CloudTable table = tableClient.GetTableReference(typeof(T).Name);
                TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
                TableResult result = await table.ExecuteAsync(retrieveOperation);
                return result.Result as T;
            }
            catch (StorageException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Select entries with query predicate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableClient"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> ExecuteQuerySegmentedAsync<T>(CloudTableClient tableClient, TableQuery query) where T : TableEntity, new()
        {
            try
            {
                // 1. Dynamic table convertation
                //var obj = DynamicTableEntityConverter.ConvertToPOCO<T>(x);
                //var segment = await table.ExecuteQuerySegmentedAsync(query, null);
                //var result = segment.Results.ToList().ConvertAll(x =>
                //{


                //    var obj = EntityPropertyConverter.ConvertBack<T>(x.Properties,new OperationContext());
                //    obj.PartitionKey = x.PartitionKey;
                //    obj.RowKey = x.RowKey;
                //    obj.Timestamp = x.Timestamp;
                //    obj.ETag = x.ETag;
                //    return obj;
                //});

                // 2. Using entity resolver;
                EntityResolver<T> cloudTableResolver = (key, rowKey, timestamp, properties, etag) =>
                {
                    T cloudEntiry = new T();
                    cloudEntiry.PartitionKey = key;
                    cloudEntiry.RowKey = rowKey;
                    cloudEntiry.Timestamp = timestamp;
                    cloudEntiry.ETag = etag;
                    cloudEntiry.ReadEntity(properties, null);
                    return cloudEntiry;
                };

                CloudTable table = tableClient.GetTableReference(typeof(T).Name);
                await table.CreateIfNotExistsAsync();
                var segment = await table.ExecuteQuerySegmentedAsync<T>(query, cloudTableResolver, null);
                return segment;
            }
            catch (StorageException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }







        /// <summary>
        /// The Table Service supports two main types of insert operations.
        ///  1. Insert - insert a new entity. If an entity already exists with the same PK + RK an exception will be thrown.
        ///  2. Replace - replace an existing entity. Replace an existing entity with a new entity.
        ///  3. Insert or Replace - insert the entity if the entity does not exist, or if the entity exists, replace the existing one.
        ///  4. Insert or Merge - insert the entity if the entity does not exist or, if the entity exists, merges the provided entity properties with the already existing ones.
        /// </summary>
        /// <param name="tableClient"></param>
        /// <param name="entity">The entity to insert or merge</param>
        /// <returns>A Task object</returns>
        //public static async Task<T> InsertOrMergeEntityAsync(CloudTable table, T entity)
        public static async Task<T> InsertOrMergeEntityAsync<T>(CloudTableClient tableClient, T entity) where T : TableEntity
        {
            try
            {
                // Get table of necessary type
                CloudTable table = tableClient.GetTableReference(typeof(T).Name);

                // Check if table exists
                await table.CreateIfNotExistsAsync();

                // Create the InsertOrReplace table operation
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge(entity);

                // Execute the operation.
                TableResult result = await table.ExecuteAsync(insertOrMergeOperation);
                return result.Result as T;
            }
            catch (StorageException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="table">Sample table name</param>
        /// <param name="deleteEntity">Entity to delete</param>
        /// <returns>A Task object</returns>
        //public static async Task DeleteEntityAsync(CloudTable table, T deleteEntity)
        public static async Task DeleteEntityAsync<T>(CloudTable table, T deleteEntity) where T : TableEntity
        {
            try
            {
                TableOperation deleteOperation = TableOperation.Delete(deleteEntity);
                await table.ExecuteAsync(deleteOperation);
            }
            catch (StorageException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Check if given connection string is for Azure Table storage or Azure CosmosDB Table.
        /// </summary>
        /// <returns>true if azure cosmosdb table</returns>
        public static bool IsAzureCosmosdbTable()
        {
            throw new NotImplementedException();
            //string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");
            //return !String.IsNullOrEmpty(connectionString) && connectionString.Contains("table.cosmosdb");
        }
    }
}
