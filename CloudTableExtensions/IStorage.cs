﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CloudTableExtension
{
    /// <summary>
    /// Initial Interface of Microsoft.Bot.Builder (no changes made)
    /// </summary>
    public interface IStorage
    {
        /// <summary>Reads storage items from storage.</summary>
        /// <param name="keys">keys of the <see cref="T:Microsoft.Bot.Builder.IStoreItem" /> objects to read.</param>
        /// <param name="cancellationToken">A cancellation token that can be used by other objects
        /// or threads to receive notice of cancellation.</param>
        /// <returns>A task that represents the work queued to execute.</returns>
        /// <remarks>If the activities are successfully sent, the task result contains
        /// the items read, indexed by key.</remarks>
        /// <seealso cref="M:Microsoft.Bot.Builder.IStorage.DeleteAsync(System.String[],System.Threading.CancellationToken)" />
        /// <seealso cref="M:Microsoft.Bot.Builder.IStorage.WriteAsync(System.Collections.Generic.IDictionary{System.String,System.Object},System.Threading.CancellationToken)" />
        Task<IDictionary<string, object>> ReadAsync(string[] keys, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>Writes storage items to storage.</summary>
        /// <param name="changes">The items to write, indexed by key.</param>
        /// <param name="cancellationToken">A cancellation token that can be used by other objects
        /// or threads to receive notice of cancellation.</param>
        /// <returns>A task that represents the work queued to execute.</returns>
        /// <seealso cref="M:Microsoft.Bot.Builder.IStorage.DeleteAsync(System.String[],System.Threading.CancellationToken)" />
        /// <seealso cref="M:Microsoft.Bot.Builder.IStorage.ReadAsync(System.String[],System.Threading.CancellationToken)" />
        Task WriteAsync(IDictionary<string, object> changes, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>Deletes storage items from storage.</summary>
        /// <param name="keys">keys of the <see cref="T:Microsoft.Bot.Builder.IStoreItem" /> objects to delete.</param>
        /// <param name="cancellationToken">A cancellation token that can be used by other objects
        /// or threads to receive notice of cancellation.</param>
        /// <returns>A task that represents the work queued to execute.</returns>
        /// <seealso cref="M:Microsoft.Bot.Builder.IStorage.ReadAsync(System.String[],System.Threading.CancellationToken)" />
        /// <seealso cref="M:Microsoft.Bot.Builder.IStorage.WriteAsync(System.Collections.Generic.IDictionary{System.String,System.Object},System.Threading.CancellationToken)" />
        Task DeleteAsync(string[] keys, CancellationToken cancellationToken = default(CancellationToken));
    }
}
