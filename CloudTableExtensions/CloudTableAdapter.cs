﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CloudTableExtension;
using Microsoft.WindowsAzure.Storage.Table;

namespace CloudTableExtensions
{
    public class CloudTableAdapter : IStorage
    {
        private CloudTableClient _ctc;
        public CloudTableAdapter(CloudTableClient ctc)
        {
            this._ctc = ctc;

        }

        public Task<IDictionary<string, object>> ReadAsync(string[] keys, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new System.NotImplementedException();
        }

        public Task WriteAsync(IDictionary<string, object> changes, CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(string[] keys, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new System.NotImplementedException();
        }

        public Task<TableQuerySegment> QuieryTable(string tableName, TableQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var exQuery = new TableQuery().Where(query.FilterString);
            var table = _ctc.GetTableReference(tableName);
            return table.ExecuteQuerySegmentedAsync(exQuery, null);
        }
    }
}
